﻿using UnityEngine;
using System.Collections;

public class Background_Game : MonoBehaviour
{

    Pause_Manager pauseManager;
    Textures texture;

    Renderer renderer;

    public static float SPEED;

    private float guardPos;

    void Start()
    {
        this.gameObject.AddComponent<SpriteRenderer>();

        this.pauseManager = Pause_Manager.getInstance();
        this.texture = Textures.getInstance();

        this.renderer = GetComponent<Renderer>();

        SPEED = -.07f;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.Background_Game;
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 1;

        this.guardPos = this.transform.position.y + (this.texture.Background_Game.rect.height / 100);

        this.gameObject.transform.position = new Vector2(0, (gameObject.name.EndsWith("0")) ? this.guardPos : 0);
    }
    void LateUpdate()
    {
        if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
        {
            transform.Translate(0, SPEED, 0);

            for (int i = 0; i < 2; i++)
                if (GameObject.Find("bg" + i))
                    if (GameObject.Find("bg" + i).gameObject.transform.position.y + (this.texture.Background_Game.rect.height / 110) < Camera.main.rect.y - Camera.main.rect.height)
                        GameObject.Find("bg" + i).transform.position = new Vector2(this.transform.position.x, GameObject.Find("bg" + ((i == 0) ? 1 : 0)).transform.position.y + (this.texture.Background_Game.rect.height / 100));
        }
    }
}
