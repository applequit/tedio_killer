﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Pause_Counter : MonoBehaviour {
    public static Pause_Counter instance;

    public static GameObject CounterPrefab;
    private GameObject CounterInstance;
    public bool started = false;
    private static Pause_Manager pauseManager;

    void Awake()
    {
        instance = this;
    }

    public bool getStarted() { return started; }

    public static void StartCounter()
    {
        if (instance == null)
        {
            GameObject obj = new GameObject("Pause_Counter_Manager");
            instance = obj.AddComponent<Pause_Counter>();
        }

        if (CounterPrefab == null)
            CounterPrefab = Resources.Load<GameObject>("Prefabs/Pause_Counter");
        if (pauseManager == null)
            pauseManager = Pause_Manager.getInstance();

        if(!instance.started) {
            instance.StartCoroutine(instance.Countdown());
        }
    }

    private IEnumerator Countdown()
    {
        started = true;
        CounterInstance = Instantiate(CounterPrefab) as GameObject;
        GameObject obj = CounterInstance.transform.Find("Counter").gameObject;
        Text text = obj.GetComponent<Text>();
        text.text = "3";
        yield return new WaitForSeconds(1f);
        text.text = "2";
        yield return new WaitForSeconds(1f);
        text.text = "1";
        yield return new WaitForSeconds(1f);
        Destroy(CounterInstance);
        pauseManager.setBool(false);
        started = false;
    }
}
