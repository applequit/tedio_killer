﻿using UnityEngine;
using System.Collections;

public class Pause_Manager {
    private static Pause_Manager instance = null;

    public static Pause_Manager getInstance()
    {
        if (instance == null)
            instance = new Pause_Manager();

        return instance;
    }

    private bool actived;

    public void setBool(bool actived) { this.actived = actived; }
    public bool getBool() { return actived; }

    public Pause_Manager()
    {
        this.setBool(false);
    }
}
