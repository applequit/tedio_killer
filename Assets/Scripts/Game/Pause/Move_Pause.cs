﻿using UnityEngine;
using System.Collections;

public class Move_Pause : MonoBehaviour
{
    public static int ID;
    public static bool isStop;

    void Start()
    {
        ID = 0;
        isStop = true;
    }

    void Update()
    {
        switch (ID)
        {
            case 1:
                if (this.transform.position.x > -6.3f)
                {
                    transform.Translate(-5f * Time.deltaTime, 0, 0);
                    isStop = false;
                }
                else
                    isStop = true;
                break;

            case 2:
                if (this.transform.position.x <= -0.1f)
                {
                    transform.Translate(5f * Time.deltaTime, 0, 0);
                    isStop = false;
                }
                else
                    isStop = true;
                break;
        }
    }
}
