﻿using UnityEngine;
using System.Collections;

public class Buttons_Pause : MonoBehaviour
{
    Analytics_Manager analyticsManager;
    Key_Manager kManager;
    Config_Select cSelect;
    Pause_Manager pauseManager;
    Textures texture;
    Sound_Manager sm;

    Vector2[] positions = new Vector2[]{
        new Vector2(4,-4.8f), //Back
        new Vector2(0, -1.7f), //Engrenagem
        new Vector2(0, -.5f), //Menu
        new Vector2(0,.5f), //Return
        new Vector2(4.6f,-2.2f), //Touch
        new Vector2(8f,-2), //Acelerometro
        new Vector2(8.3f,1.85f), // Musica        
        new Vector2(4.8f,1.7f), // Audio
    };

    GameObject g;

    float[] correction = new float[]{
        .46f, .37f
    };

    int index;
    bool canRotate;

    void Start()
    {
        this.canRotate = false;
        this.index = 0;

        this.transform.parent = GameObject.Find("Pause").transform;

        this.analyticsManager = Analytics_Manager.getInstance();
        this.kManager = Key_Manager.getInstance();
        this.cSelect = new Config_Select();
        this.pauseManager = Pause_Manager.getInstance();
        this.texture = Textures.getInstance();
        this.sm = Sound_Manager.getInstance();

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<BoxCollider2D>();

        this.GetComponent<SpriteRenderer>().sprite = texture.b_pause[int.Parse(this.gameObject.name.Substring(1))];
        this.GetComponent<SpriteRenderer>().sortingOrder = 7;

        this.GetComponent<BoxCollider2D>().size = new Vector2(texture.b_pause[int.Parse(this.gameObject.name.Substring(1))].rect.width / 100,
                                                              texture.b_pause[int.Parse(this.gameObject.name.Substring(1))].rect.height / 100);

        this.GetComponent<SpriteRenderer>().enabled = false;
        this.GetComponent<BoxCollider2D>().enabled = false;


        this.transform.position = this.positions[int.Parse(this.gameObject.name.Substring(1))];

        switch(this.gameObject.name.Substring(1))
        {
            case "7"://sound
                this.GetComponent<SpriteRenderer>().sprite = texture.b_pause[int.Parse(this.gameObject.name.Substring(1)) + ((this.sm.getID() == 0) ? 2 : 0)];
                print("value: "+this.sm.getID());
                break;
        }
    }

    void Update()
    {
        if (pauseManager.getBool())
            this.disable(true);
        else
            this.disable(false);

        if (this.canRotate)
            transform.Rotate(0, 0, -Time.deltaTime * 100);

        if (this.transform.eulerAngles.z < 150)
        {
            this.canRotate = false;
            this.transform.eulerAngles = Vector3.zero;
        }
    }

    void OnMouseDown()
    {
        if (Move_Pause.isStop) this.transform.localScale = new Vector2(1.3f, 1.3f);
    }

    void OnMouseUp()
    {
        if(Move_Pause.isStop && GameObject.Find("Pause_Counter_Manager") == null)
        {
            this.transform.localScale = Vector2.one;

            switch (int.Parse(this.gameObject.name.Substring(1)))
            {
                case 0:
                    Move_Pause.ID = 2;
                    break;
                case 1: 
                    if (this.gameObject.name.EndsWith("1") && this.transform.eulerAngles.z < 200)
                    Move_Pause.ID = 1;
                    break;

                case 2:
                    this.g = new GameObject("fOut", typeof(Fade_Out));
                    pauseManager.setBool(false);
                    this.analyticsManager.sendScene(0);
                    break;

                case 3:
                    pauseManager.setBool(false);
                    this.analyticsManager.sendScene(1);
                    Pause_Counter.StartCounter();
                    break;
            }

            if (this.gameObject.name.EndsWith("1"))
                this.canRotate = true;

            if (int.Parse(this.gameObject.name.Substring(1)) > 5)
            {
                this.index = (this.index == 0) ? 2 : 0;
                this.GetComponent<SpriteRenderer>().sprite = texture.b_pause[int.Parse(this.gameObject.name.Substring(1)) + index];

                switch (this.gameObject.name.Substring(1))
                {
                    case "7"://sound
                        this.sm.setID((index==2)?1:0);
                        this.GetComponent<SpriteRenderer>().sprite = texture.b_pause[int.Parse(this.gameObject.name.Substring(1)) + ((this.sm.getID() == 0) ? 2 : 0)];
                        print("value: " + this.sm.getID());
                        break;
                }
            }

            if (int.Parse(this.gameObject.name.Substring(1)) < 6 && int.Parse(this.gameObject.name.Substring(1)) > 3)
            {
                Destroy(GameObject.Find("slct"));
                this.cSelect.setSelect(this.g, new Vector2(this.transform.position.x + this.correction[int.Parse(this.gameObject.name.Substring(1))-4], -1.5f), "slct");

                this.kManager.setKey(int.Parse(this.gameObject.name.Substring(1))-4);
            }

        }
    }

    private void disable(bool b)
    {
        this.GetComponent<SpriteRenderer>().enabled = b;
        this.GetComponent<BoxCollider2D>().enabled = b;
    }
}
