﻿using UnityEngine;
using System.Collections;

public class Background_Pause : MonoBehaviour
{

    Pause_Manager pauseManager;
    Textures texture;
    float alpha;

    void Start()
    {
        this.alpha = 0;

        this.StartCoroutine(this.fadeIn());
        this.StartCoroutine(this.fadeOut());

        this.gameObject.AddComponent<SpriteRenderer>();

        this.pauseManager = Pause_Manager.getInstance();
        this.texture = Textures.getInstance();

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.title_pause[int.Parse(this.gameObject.name.Substring(6))];
        this.setAlpha(this.alpha);
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 6 + (int.Parse(this.gameObject.name.Substring(6)));
        this.GetComponent<SpriteRenderer>().enabled = false;

        this.transform.position = new Vector2(3.4f + (int.Parse(this.gameObject.name.Substring(6)) * 3f), 0);
        this.transform.parent = GameObject.Find("Pause").transform;
        this.transform.localScale = new Vector2(1.05f, 1.05f);
    }

    void Update()
    {
        if (pauseManager.getBool())
            this.disable(true);
        //else
            //this.disable(false);
    }

    private void disable(bool b)
    {
        this.GetComponent<SpriteRenderer>().enabled = b;
        //this.GetComponent<BoxCollider2D>().enabled = b;
    }

    IEnumerator fadeIn()
    {
        while (true)
        {
            yield return new WaitForSeconds(.01f);
            if (pauseManager.getBool())
            {
                if (this.alpha < 1)
                    this.alpha += .05f;

                this.setAlpha(this.alpha);
            }
        }
    }

    IEnumerator fadeOut()
    {
        while (true)
        {
            yield return new WaitForSeconds(.01f);
            if (!pauseManager.getBool())
            {
                if (this.alpha > 0)
                    this.alpha -= .05f;

                this.setAlpha(this.alpha);
            }
        }
    }

    private void setAlpha(float alpha) { this.gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, alpha); }
}
