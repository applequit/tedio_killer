﻿using UnityEngine;
using System.Collections;

public class Interface_Game : MonoBehaviour
{
    Analytics_Manager analyticsManager;
    Textures texture;
    Sound_Manager sManager;
    Pause_Manager pauseManager;

    Vector2[] pos = new Vector2[]{
        new Vector2(-2.1f, 4.6f),
        new Vector2(-2.3f, 5.15f),
        new Vector2(2.7f, 5),
        new Vector2(1.5f,5)
    };

    void Start()
    {
        this.analyticsManager = Analytics_Manager.getInstance();
        this.pauseManager = Pause_Manager.getInstance();
        this.sManager = Sound_Manager.getInstance();
        this.texture = Textures.getInstance();

        gameObject.AddComponent<SpriteRenderer>();

        if (int.Parse(gameObject.name.Substring(2)) > 1)
            gameObject.AddComponent<BoxCollider2D>();

        gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.b_interface[int.Parse(gameObject.name.Substring(2))];

        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 4;

        if (int.Parse(gameObject.name.Substring(2)) > 1)
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.texture.b_interface[int.Parse(gameObject.name.Substring(2))].rect.width / 100,
                                                                        this.texture.b_interface[int.Parse(gameObject.name.Substring(2))].rect.height / 100);


        this.transform.position = this.pos[int.Parse(gameObject.name.Substring(2))];

        if (this.gameObject.name.Substring(2) == "3")
            gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.b_interface[this.sManager.getID() + 3];
    }

    void Update()
    {
        if (this.gameObject.name.Substring(2) == "3")
            gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.b_interface[this.sManager.getID() + 3];
    }

    void OnMouseDown()
    {
        switch (int.Parse(gameObject.name.Substring(2)))
        {
            case 2:
                break;
            case 3:
                break;
        }
    }

    void OnMouseUp()
    {
        if (!GameObject.FindObjectOfType<Pause_Counter>().started && !this.pauseManager.getBool())
        {
            switch (int.Parse(gameObject.name.Substring(2)))
            {
                case 2:
                    pauseManager.setBool(true);
                    this.analyticsManager.sendScene(99);
                    break;
                case 3:
                    this.sManager.setID((this.sManager.getID() == 0) ? 1 : 0);
                    break;
            }
        }
    }
}
