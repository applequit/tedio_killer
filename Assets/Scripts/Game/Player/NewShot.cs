﻿using UnityEngine;
using System.Collections;

public class NewShot : MonoBehaviour
{
    private Vector3 myAngle;
    private Vector2 myPosition;

    Pause_Manager pauseManager;

    void Start()
    {
        this.pauseManager = Pause_Manager.getInstance();

        switch (GameObject.FindObjectOfType<Player>().getIndPower())
        {
            case 2:
                this.FixConfig("shot_zurreal");
                break;

            default:
                this.FixConfig("shot_triple");
                break;
        }

        this.transform.position = this.myPosition;
        this.transform.eulerAngles = this.myAngle;
    }

    void Update()
    {
        if (this.transform.position.y > 5.6F || this.transform.position.y < -5.6f)
            Destroy(this.gameObject);
    }

    void FixedUpdate()
    {
        if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            this.transform.Translate(0, Time.deltaTime * 10, 0);
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name.StartsWith("obj"))
        {
            Destroy(this.gameObject);
            if (this.gameObject.name.Equals("especial"))
                Destroy(c.gameObject);
        }
    }

    private void FixConfig(string s)
    {
        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load("Images/Game/Main/PowerUps/" + s, typeof(Sprite)) as Sprite;
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 6;

        this.gameObject.AddComponent<BoxCollider2D>();
        this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;

        this.gameObject.AddComponent<Rigidbody2D>();
        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
    }

    public void setPosition(Vector2 pos) { this.myPosition = pos; }
    public void setAngle(Vector3 angle) { this.myAngle = angle; }
}
