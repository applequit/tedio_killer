﻿using UnityEngine;
using System.Collections;

public class Interface_PowerUp : MonoBehaviour {

    private int id;
    public bool canDestroy;

    Pause_Manager pauseManager;

    void Start () {
        this.pauseManager = Pause_Manager.getInstance();

        switch(this.id)
        {
            case 0:
                this.FixConfig("bg",6);
                break;
            case 2:
                this.FixConfig("icon_zurreal",7);
                break;

            default:
                this.FixConfig("icon_triple",7);
                break;
        }

        this.StartCoroutine(this.time2destroy());
	}
	
	void Update () {
        if (this.canDestroy) Destroy(this.gameObject);
	}

    #region configuração
    private void FixConfig(string s, int order)
    {
        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load("Images/Game/Main/PowerUps/" + s, typeof(Sprite)) as Sprite;
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = order;

        this.gameObject.AddComponent<BoxCollider2D>();
        this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;

        this.gameObject.AddComponent<Animator>();
        this.gameObject.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)RuntimeAnimatorController.Instantiate(Resources.Load("Animations/PowerUp/Interface/bg_pup"));


        this.gameObject.AddComponent<Rigidbody2D>();
        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;

        this.transform.position = new Vector2(-2.15f, 3.7f);
    }
    #endregion

    public void setID(int id) { this.id = id; }


    #region temporizador
    IEnumerator time2destroy()
    {
        while(true)
        {
            yield return new WaitForSeconds(2);
            if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            this.gameObject.GetComponent<Animator>().SetBool("canOut", true);
        }
    }
    #endregion
}
