﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour
{
    Pause_Manager pauseManager;
    void Start()
    {
        this.pauseManager = Pause_Manager.getInstance();

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load("Images/Game/Main/PowerUps/powerup_main", typeof(Sprite)) as Sprite;
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 5;

        this.gameObject.AddComponent<BoxCollider2D>();
        this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;

        this.gameObject.AddComponent<Rigidbody2D>();
        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;

        this.gameObject.AddComponent<Animator>();
        this.gameObject.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)RuntimeAnimatorController.Instantiate(Resources.Load("Animations/PowerUp/Main/power"));

        this.transform.position = new Vector2(Random.Range(2.5f, -2.5f), 8);
        this.transform.localScale = new Vector2(.8f, .8f);
        //print(this.gameObject.name);
    }

    void Update()
    {
        if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted()) this.gameObject.GetComponent<Animator>().StopPlayback();
        else this.gameObject.GetComponent<Animator>().StartPlayback();
    }

    void LateUpdate()
    {
        if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            transform.Translate(0, Background_Game.SPEED, 0); //default
        this.clear();
    }


    void clear() { if (this.transform.position.y < -5.7f) Destroy(this.gameObject); }
}
