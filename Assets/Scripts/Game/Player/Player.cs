﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    GameObject g;

    Wave_Manager wManager;
    Textures texture;
    Enemy_Manager eManager;
    Pause_Manager pauseManager;
    Key_Manager keyManager;
    Identity identity;

    Sprite spriteInit, spriteDefault;
    // "rabiskum", "esboçoria", "illustrator"
    private int id, damage, ind_power;

    private bool iamAlive, iamPowerful;

    void Start()
    {
        this.id = 1;
        this.ind_power = 0;

        this.iamAlive = true;
        this.iamPowerful = false;

        this.wManager = Wave_Manager.getInstance();
        this.eManager = Enemy_Manager.getInstance();
        this.pauseManager = Pause_Manager.getInstance();
        this.keyManager = Key_Manager.getInstance();
        this.identity = Identity.getInstance();

        this.spriteInit = Resources.Load("Images/Game/Main/Player/" + this.identity.Skin + "/nave" + (this.id + 1), typeof(Sprite)) as Sprite;
        this.spriteDefault = Resources.Load("Images/Game/Main/Player/rabiskum/nave1", typeof(Sprite)) as Sprite;

        this.StartCoroutine(this.shot());
        this.StartCoroutine(this.anim_player());

        gameObject.AddComponent<SpriteRenderer>();

        this.texture = Textures.getInstance();

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.spriteInit;
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 6;

        gameObject.AddComponent<PolygonCollider2D>();
        this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;

        this.transform.position = new Vector2(0, -8);

        this.transform.localScale = new Vector2(.8f, .8f);
    }

    void Update()
    {
        if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
        {
            if (this.transform.position.y < -4.3f)
                transform.Translate(0, .25f * (Time.deltaTime * 10), 0);
            else
                this.keyManager.main_Manager(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name.StartsWith("obj"))
            this.iamAlive = false;

        if (c.name.StartsWith("e") || c.name.StartsWith("shot") && !c.name.EndsWith("0"))
        {
            this.damage += (c.name.StartsWith("e")) ? this.eManager.getDamage(int.Parse(c.gameObject.name.Substring(2)), this.wManager.getNivel()) : 1;
            if (this.damage >= this.identity.getResistance())
                this.iamAlive = false;
            Destroy(c.gameObject);
        }

        if (c.name.Equals("power"))//coletou poder
        {
            this.ind_power = Random.Range(1, 10);

            if (GameObject.Find("bg_pup") == null)
            {
                this.g = new GameObject("bg_pup", typeof(Interface_PowerUp));
                this.g.GetComponent<Interface_PowerUp>().setID(0);

                this.g = new GameObject("icon_pup", typeof(Interface_PowerUp));
                this.g.GetComponent<Interface_PowerUp>().setID(this.ind_power);
            }

            this.iamPowerful = true;
            Destroy(c.gameObject);
        }
    }

    void call_Menu()
    {
        Destroy(this.gameObject);
        Application.LoadLevel("Lose");

    }

    public int getIndPower() { return this.ind_power; }

    #region animação player
    int idExplosion;
    IEnumerator anim_player()
    {
        while (true)
        {
            yield return new WaitForSeconds(.09F);
            if (!pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            {
                if (this.iamAlive)
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load("Images/Game/Main/Player/" + this.identity.Skin + "/nave" + (this.id = (this.id < 3) ? this.id += 1 : 1), typeof(Sprite)) as Sprite;
                else
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load("Images/Game/Main/Player/explosion/e" + ((this.idExplosion < 7) ? this.idExplosion += 1 : 6), typeof(Sprite)) as Sprite;

                if (this.idExplosion >= 6)
                    this.call_Menu();
            }
        }
    }
    #endregion

    #region gerencia do tiro do player

    float aux_sec;
    IEnumerator shot()
    {
        while (true)
        {

            yield return new WaitForSeconds(this.identity.getShootingRate());
            if (!pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            {
                if (this.iamPowerful)
                {
                    switch (this.ind_power)
                    {
                        case 2:
                            g = new GameObject("especial", typeof(NewShot));
                            g.GetComponent<NewShot>().setPosition(new Vector2(this.transform.position.x, this.transform.position.y + .15f));
                            break;

                        default:
                            g = new GameObject("shot0", typeof(NewShot));
                            g.GetComponent<NewShot>().setPosition(new Vector2(this.transform.position.x, this.transform.position.y + .15f));

                            yield return new WaitForSeconds(.1f);
                            g = new GameObject("shot0", typeof(NewShot));
                            g.GetComponent<NewShot>().setPosition(new Vector2(this.transform.position.x - .1f, this.transform.position.y + .15f));
                            g.GetComponent<NewShot>().setAngle(new Vector3(0, 0, -15));


                            g = new GameObject("shot0", typeof(NewShot));
                            g.GetComponent<NewShot>().setPosition(new Vector2(this.transform.position.x + .1f, this.transform.position.y + .15f));
                            g.GetComponent<NewShot>().setAngle(new Vector3(0, 0, 15));
                            break;

                    }

                    this.aux_sec = ((this.aux_sec <= 10) ? (this.aux_sec += (1 / this.identity.getShootingRate())) : 10);

                    if ((int)this.aux_sec >= 10)
                    {
                        this.iamPowerful = false;
                        this.aux_sec = 0;
                    }

                }

                else
                {
                    g = new GameObject("shot0", typeof(Shot));
                    g.GetComponent<Shot>().setIMG("tiro" + (identity.getSelect() + 1));
                    g.GetComponent<Shot>().setPosition(new Vector2(this.transform.position.x, this.transform.position.y + 1.15f));
                }
            }
        }
    }
    #endregion
}
