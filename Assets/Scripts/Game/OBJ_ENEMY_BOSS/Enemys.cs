﻿using UnityEngine;
using System.Collections;

public class Enemys : MonoBehaviour 
{
    Wave_Manager wManager;
    Identity identity;
    Textures texture;
    Enemy_Manager eManager;
    Enemys_Behaviour eBehaviour;
    Pause_Manager pManager;

    GameObject g;

    private int anim_skin, id_skin, damage, speed_angle;
    private float speed_horizontal;

    private bool horizontalBool;

    void Awake()
    {
        this.damage = 0;
    }

    void Start()
    {
        this.horizontalBool = false;

        this.speed_horizontal = .25f;
        this.speed_angle = 10;
        this.id_skin = int.Parse(this.gameObject.name.Substring(2));
        this.anim_skin = int.Parse(this.gameObject.name.Substring(2, 1)) * 3;

        this.wManager = Wave_Manager.getInstance();
        this.identity = Identity.getInstance();
        this.pManager = Pause_Manager.getInstance();
        this.eManager = Enemy_Manager.getInstance();
        this.texture = Textures.getInstance();
        this.eBehaviour = Enemys_Behaviour.getInstance();

        this.StartCoroutine(this.run_anim());
        this.StartCoroutine(this.shot());

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<Rigidbody2D>();

        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 4;

        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;

        this.eManager.setPosition(this.gameObject, this.wManager.getNivel());
        this.transform.localScale = new Vector2(.65f, .65f);

        this.gameObject.tag = "enemy";

    }

    void LateUpdate()
    {
        switch (this.gameObject.name.Substring(1, 1))
        {
            case "m":
                this.setEnemy(this.texture.enemys_math, this.anim_skin);
                break;
            case "p":
                this.setEnemy(this.texture.enemys_physic, this.anim_skin);
                break;
            case "c":
                this.setEnemy(this.texture.enemys_chemistry, this.anim_skin);
                break;
        }
        this.clear();
        this.toMove();

    }

    void toMove()
    {
        if (!pManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
        {
            this.eBehaviour.behaviours(this.gameObject, this.wManager.getNivel(), speed_angle);
            transform.Translate(0, Background_Game.SPEED, 0); //default

            if (this.horizontalBool)
            {
                if (transform.position.x <= -2.7f || transform.position.x >= 2.7f)
                    this.speed_horizontal *= -1;
                this.transform.Translate(this.speed_horizontal * (Time.deltaTime * 10), 0, 0);
            }
        }
    }

    void clear() { if (this.transform.position.y < -5.7f) Destroy(this.gameObject); }

    private void setEnemy(Sprite[] sprite, int id)
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = sprite[id];

        if (!this.gameObject.GetComponent<PolygonCollider2D>())
        {
            gameObject.AddComponent<PolygonCollider2D>();
            this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
        }
    }

    public void setDamage(int d) { this.damage = d; }
    public int getDamage() { return this.damage; }
    public int getSkin() { return this.id_skin; }
    public void setHorizontal(bool b) { this.horizontalBool = b; }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name.StartsWith("shot0") || c.name.Equals("especial"))
        {
            this.damage += this.identity.getDamage();
            if (this.getDamage() >= this.eManager.getResistance(int.Parse(this.gameObject.name.Substring(2)), this.wManager.getNivel()))
                Destroy(this.gameObject);

            print(this.gameObject.name + ", " + damage);
            Destroy(c.gameObject);
        }
    }

    IEnumerator run_anim()
    {
        while (true)
        {
            yield return new WaitForSeconds(.1f);
            if (!this.pManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            {
                // 3~FRAMES
                for (int i = 0; i < 4; i++) //(i * 3) + 1
                    if (this.id_skin == i)
                        this.anim_skin = (this.anim_skin > ((i * 3) + 1)) ? (i * 3) : this.anim_skin += 1;
            }
        }
    }

    IEnumerator shot()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);

            bool b = false;

            if (this.wManager.getNivel() == "physic" && this.gameObject.name.Substring(2) == "1" ||
                this.wManager.getNivel() == "chemistry" && this.gameObject.name.Substring(2) == "3")
                b = true;

            if (b)
            {
                g = new GameObject("shot1", typeof(Shot));
                g.GetComponent<Shot>().setPosition(new Vector2(this.transform.position.x, this.transform.position.y - .5f));
            }
        }
    }
}
