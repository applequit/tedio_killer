﻿using UnityEngine;
using System.Collections;

public class Obj_Manager
{

    private static Obj_Manager instance = null;

    float RPencil, RRotate;
    Vector2[] RandomRotate = new Vector2[]{
        Vector2.zero,
        new Vector2(0,180)
    };

    public static Obj_Manager getInstance()
    {
        if (instance == null)
            instance = new Obj_Manager();

        return instance;
    }

    public Obj_Manager()
    {

    }

    public void main_manager(GameObject g)
    {
        if ((int.Parse(g.gameObject.name.Substring(3))) == 2 || (int.Parse(g.gameObject.name.Substring(3))) == 3) //lápis
        {
            this.random_rotate(g);

            switch ((int)g.transform.eulerAngles.y)
            {
                case 0:
                    this.RPencil = Random.Range(-2.7f, -1f);
                    break;
                case 180:
                    this.RPencil = Random.Range(1f, 2.7f);
                    break;
            }

            this.setPosition(g, this.RPencil);
        }

        if ((int.Parse(g.gameObject.name.Substring(3))) == 0 || (int.Parse(g.gameObject.name.Substring(3))) == 1) //borracha
        {
            this.random_rotate(g);
            this.RPencil = Random.Range(-2.15f, 2.15f);
            this.setPosition(g, this.RPencil);
        }
    }

    private void setPosition(GameObject g, float R) { g.transform.position = new Vector2(R, 7); }

    private void random_rotate(GameObject g)
    {
        this.RRotate = Random.Range(0, 2);
        g.transform.eulerAngles = this.RandomRotate[(int)this.RRotate];
    }
}
