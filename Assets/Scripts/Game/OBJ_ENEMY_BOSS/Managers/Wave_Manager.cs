﻿using UnityEngine;
using System.Collections;

public class Wave_Manager
{
    private static Wave_Manager instance = null;

    public static Wave_Manager getInstance()
    {
        if (instance == null)
            instance = new Wave_Manager();
        return instance;
    }

    private int N_Wave, sec;
    private string nivel;

    public Wave_Manager()
    {
        this.N_Wave = 0;
        this.nivel = "math"; //math, physic, chemistry
    }

    public float getSpawn()
    {
        switch (this.getWave())
        {
            case 1:
                return .5f;
            case 2:
                return 1.7f;
        }

        return .5f;
    }

    public int getNumber()
    {
        switch (this.getNivel())
        {
            case "math":
            case "chemistry":
                return 4;
            case "physic":
                return 3;
        }
        return 0;
    }

    public void setNextLevel(int sec)
    {
        if (sec == 180)
        {
            switch (this.getNivel())
            {
                case "math":
                    this.setWave(0);
                    this.setNivel("physic");
                    break;
                case "physic":
                    this.setWave(0);
                    this.setNivel("chemistry");
                    break;
                case "chemistry":
                    break;
            }
        }

        this.sec = sec;
    }

    public int getUnlock(int r)
    {
        if (this.sec >= 60)
        {
            switch (this.getNivel())
            {
                case "math":
                    return 2;
                case "physic":
                    return 3;
                case "chemistry":
                    return 4;
            }
        }
        return r;
    }

    public int getNumberNivel()
    {
        switch (this.getNivel())
        {
            case "math": return 1;
            case "physic": return 2;
            case "chemistry": return 3; 
        }
        return 0;
    }

    public void setNivel(string s) { this.nivel = s; }
    public string getNivel() { return this.nivel; }

    public void setWave(int W) { this.N_Wave = W; }
    public int getWave() { return this.N_Wave; }
}
