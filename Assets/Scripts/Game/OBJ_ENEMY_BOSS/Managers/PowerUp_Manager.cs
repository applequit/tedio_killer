﻿using UnityEngine;
using System.Collections;

public class PowerUp_Manager {

    private static PowerUp_Manager instance = null;

    public static PowerUp_Manager getInstance()
    {
        if (instance == null)
            instance = new PowerUp_Manager();
        return instance;
    }

    private int n_powers;

    public PowerUp_Manager()
    {
        this.n_powers = 2;
    }

    public int getNPowers() { return n_powers; }
}
