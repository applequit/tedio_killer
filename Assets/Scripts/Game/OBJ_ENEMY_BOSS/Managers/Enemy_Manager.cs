﻿using UnityEngine;
using System.Collections;

public class Enemy_Manager
{
    private static Enemy_Manager instance = null;

    public int id_skin;

    public static Enemy_Manager getInstance()
    {
        if (instance == null)
            instance = new Enemy_Manager();
        return instance;
    }

    public Enemy_Manager()
    {
    }

    public void setPosition(GameObject g, string scene)
    {
        if (g.name.Substring(2) == "3" && scene == "math") g.transform.position = new Vector2(GameObject.FindObjectOfType<Player>().transform.position.x, 8);
        else
            g.transform.position = new Vector2(Random.Range(-2.5f, 2.5f), 10);

        //Debug.Log("cena: " + scene);
    }

    public int getDamage(int skin, string nivel)
    {
        int d = 0;

        if (nivel == "math")
        {
            switch (skin)
            {
                case 0://X
                case 1://Y
                case 2://PI
                case 3://A.RETO
                    d = 1;
                    break;
            }
        }

        if (nivel == "physic")
        {
            switch (skin)
            {
                case 0://W
                case 1://OMEGA
                case 2://LAMBDA
                    d = 1;
                    break;
            }
        }

        if (nivel == "chemistry")
        {
            switch (skin)
            {
                case 0://BENZENO
                case 1://OURO
                case 2://CARBONO
                    d = 1;
                    break;
                case 3://ATOMO
                    d = 2;
                    break;
            }
        }
        return d;
    }

    public int getResistance(int skin, string nivel)
    {
        int r = 0;

        if (nivel == "math")
        {
            switch (skin)
            {
                case 0://X
                case 1://Y
                case 2://PI
                case 3://A.RETO
                    r = 1;
                    break;
            }
        }

        if (nivel == "physic")
        {
            switch (skin)
            {
                case 0://W
                case 1://OMEGA
                    r = 1;
                    break;
                case 2://LAMBDA
                    r = 2;
                    break;
            }
        }

        if (nivel == "chemistry")
        {
            switch (skin)
            {
                case 0://BENZENO
                    r = 2;
                    break;
                case 1://OURO
                case 2://CARBONO
                    r = 3;
                    break;
                case 3://ATOMO
                    r = 2;
                    break;
            }
        }

        return r;
    }

    public int getSkin() { return id_skin; }
    public void setSkin(int skin) { this.id_skin = skin; }
}
