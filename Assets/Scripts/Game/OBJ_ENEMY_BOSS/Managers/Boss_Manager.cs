﻿using UnityEngine;
using System.Collections;

public class Boss_Manager
{

    private static Boss_Manager instance = null;

    public static Boss_Manager getInstance()
    {
        if (instance == null)
            instance = new Boss_Manager();
        return instance;
    }

    public Boss_Manager()
    {

    }

    public int getResistance(string id)
    {
        switch (id)
        {
            case "1":
                return 10;
            case "2":
                return 15;
            case "3":
                return 20;
        }
        return 0;
    }


}
