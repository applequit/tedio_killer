﻿using UnityEngine;
using System.Collections;

public class Bosses : MonoBehaviour
{
    GameObject g;

    Boss_Manager bossManager;
    Bosses_Behaviour bossBehaviour;
    Pause_Manager pManager;
    Textures textures;
    Identity identity;

    private int anim_skin, id_skin, damage;
    private float n_shot;
    private bool can_shot;

    void Start()
    {
        this.can_shot = true;
        this.damage = 0;

        this.identity = Identity.getInstance();
        this.bossManager = Boss_Manager.getInstance();
        this.bossBehaviour = Bosses_Behaviour.getInstance();
        this.pManager = Pause_Manager.getInstance();
        this.textures = Textures.getInstance();

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<Rigidbody2D>();

        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 4;

        this.StartCoroutine(this.shot());
        this.StartCoroutine(this.run_anim());

        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;

        this.anim_skin = (int.Parse(this.gameObject.name.Substring(1)) - 1) * 3;
        this.id_skin = int.Parse(this.gameObject.name.Substring(1)) - 1;
        this.n_shot = .5f;

        this.transform.position = new Vector2(0, 8);
        this.gameObject.transform.localScale = new Vector3(.5f, .5f, .5f);
        this.gameObject.tag = "boss";
    }

    void Update()
    {
        this.setEnemy(this.textures.bosses, this.anim_skin);
    }

    void LateUpdate()
    {
        if (!this.pManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            this.bossBehaviour.general_behaviour(this.gameObject, this.id_skin);
    }

    private void setEnemy(Sprite[] sprite, int id)
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = sprite[id];

        if (!this.gameObject.GetComponent<PolygonCollider2D>())
        {
            gameObject.AddComponent<PolygonCollider2D>();
            this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name.StartsWith("shot0") || c.name.Equals("especial"))
        {
            this.damage += (c.name.Equals("especial")?this.identity.getDamage()+2:this.identity.getDamage());
            if (this.damage >= this.bossManager.getResistance(this.gameObject.name.Substring(1)))
            {
                Destroy(this.gameObject);
                GameObject.FindObjectOfType<Create>().setStatusBoss(true);

                if (this.gameObject.name.Substring(1) == "3")//END GAME
                    Application.LoadLevel("Win");
            }

            Destroy(c.gameObject);

            print("Dano: " + this.damage);
        }
    }

    IEnumerator run_anim()
    {
        while (true)
        {
            yield return new WaitForSeconds(.1f);
            if (!this.pManager.getBool())
            {
                for (int i = 0; i < 3; i++)
                    if (this.id_skin == i)
                        this.anim_skin = (this.anim_skin < ((i + 1) * 3) - 1) ? this.anim_skin += 1 : (i * 3);
            }
        }
    }

    IEnumerator shot()
    {
        while (true)
        {
            yield return new WaitForSeconds(this.n_shot);
            if (!this.pManager.getBool())
            {
                //1~3
                if (this.can_shot)
                {
                    switch (this.gameObject.name.Substring(1))
                    {   //1 - normal // 2 - tabela //3 - rajada
                        case "1": //math
                            g = new GameObject(!bossBehaviour.getGust() ? "shot1" : "shot3", typeof(Shot));
                            g.GetComponent<Shot>().setPosition(new Vector2(this.transform.position.x, this.transform.position.y - 1.2f));
                            break;

                        case "2": //physic
                            g = new GameObject("shot1", typeof(Shot));
                            g.GetComponent<Shot>().setPosition(new Vector2(this.transform.position.x, this.transform.position.y - 1.2f));
                            break;

                        case "3": //chemistry
                            g = new GameObject("shot1", typeof(Shot));
                            g.GetComponent<Shot>().setPosition(new Vector2(this.transform.position.x, this.transform.position.y - 1.2f));
                            break;
                    }
                }
            }
        }
    }

    public void set_shot(float n) { this.n_shot = n; }
    public void set_canshot(bool b) { this.can_shot = b; }
}
