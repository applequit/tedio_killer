﻿using UnityEngine;
using System.Collections;

public class Objects : MonoBehaviour
{
    Obj_Manager objManager;
    Pause_Manager pauseManager;
    Textures texture;


    void Start()
    {
        this.objManager = Obj_Manager.getInstance();
        this.pauseManager = Pause_Manager.getInstance();
        this.texture = Textures.getInstance();

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<Rigidbody2D>();

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.objects_skin[int.Parse(this.gameObject.name.Substring(3))]; //obj
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 3;

        gameObject.AddComponent<PolygonCollider2D>();
        this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;

        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        this.gameObject.GetComponent<Rigidbody2D>().fixedAngle = true;

        this.objManager.main_manager(this.gameObject);

    }

    void LateUpdate()
    {
        this.clear();
        this.toMove();
    }

    void toMove() { if(!pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted()) transform.Translate(0, Background_Game.SPEED, 0); }
    void clear() { if (this.transform.position.y < -8) Destroy(this.gameObject); }
}
