﻿using UnityEngine;
using System.Collections;

public class Create : MonoBehaviour
{
    GameObject g;

    Wave_Manager wManager;
    Pause_Manager pauseManager;
    PowerUp_Manager pupManager;

    private bool canCountSeconds, turnBoss, bossIsDead;

    int r, second;

    public static string mode;

    void Start()
    {

        this.pupManager = PowerUp_Manager.getInstance();
        this.wManager = Wave_Manager.getInstance();
        this.pauseManager = Pause_Manager.getInstance();

        this.canCountSeconds = true;
        this.turnBoss = false;
        this.bossIsDead = false;

        this.second = 60 * this.wManager.getWave();

        switch (mode)
        {
            case "normal":
                this.StartCoroutine(this.create_obstacles(g));
                this.StartCoroutine(this.create_enemys(g));
                this.StartCoroutine(this.newWave());
                this.StartCoroutine(this.create_boss(g));
                this.StartCoroutine(this.instance_powerups(g));
                break;
            case "boss":
                this.StartCoroutine(this.create_boss(g));
                this.StartCoroutine(this.instance_powerups(g));
                break;
        }
    }

    #region criação de obstáculos (lápis e borracha)
    IEnumerator create_obstacles(GameObject g)
    {
        while (true)
        {
            yield return new WaitForSeconds(1.5F);

            if (!this.turnBoss)
                if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
                {
                    if (this.wManager.getWave() == 0 || this.wManager.getWave() == 2)
                    {
                        this.r = Random.Range(0, 3);
                        g = new GameObject("obj" + this.r, typeof(Objects));
                    }
                }
        }
    }
    #endregion

    #region criação dos inimigos
    IEnumerator create_enemys(GameObject g)
    {
        while (true)
        {
            yield return new WaitForSeconds(this.wManager.getSpawn());

            if (!this.turnBoss)
                if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
                {
                    if (this.wManager.getWave() == 1 || this.wManager.getWave() == 2)
                        g = new GameObject("e" + (this.wManager.getNivel().Substring(0, 1)) + Random.Range(0, this.wManager.getNumber()), typeof(Enemys));
                }
        }
    }
    #endregion

    #region criação dos chefes
    IEnumerator create_boss(GameObject g)
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            {
                if (mode == "normal")
                {
                    if (GameObject.FindObjectOfType<Bosses>() == null && !this.canCountSeconds && !this.bossIsDead)
                        g = new GameObject("b" + this.wManager.getNumberNivel(), typeof(Bosses));
                }
                else
                {
                    if (GameObject.FindObjectOfType<Bosses>() == null)
                        g = new GameObject("b" + this.wManager.getNumberNivel(), typeof(Bosses));
                }
            }
        }
    }
    #endregion

    #region controle de novas waves
    IEnumerator newWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            {
                for (int i = 0; i < 4; i++)
                    if (this.second == 60 * i && this.second < 60 * (i + 1))
                        this.wManager.setWave(i);

                if (this.canCountSeconds)
                    this.second = (this.second == 180) ? 0 : this.second += 1;

                if (this.second >= 180)
                    this.canCountSeconds = this.bossIsDead;

                this.turnBoss = !this.canCountSeconds;//Saber se pode aparecer o boss ou n


                if (!this.turnBoss)
                {
                    this.wManager.setNextLevel(this.second);
                }

                //for (int i = 0; i < 4; i++ )

                print("wave: " + this.wManager.getWave() + ", second: " + this.second);
            }
        }
    }
    #endregion

    #region instância de power ups
    IEnumerator instance_powerups(GameObject g)
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(15, 35));
            if (!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            {
                if (this.wManager.getWave() == 0 && this.wManager.getNivel() == "math")
                    print("abc...");
                else
                    g = new GameObject("power", typeof(PowerUp));
            }
        }
    }
    #endregion

    public void setStatusBoss(bool status)
    {
        this.bossIsDead = status;
    }
}
