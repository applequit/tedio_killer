﻿using UnityEngine;
using System.Collections;

public class Enemys_Behaviour
{

    private static Enemys_Behaviour instance = null;

    public static Enemys_Behaviour getInstance()
    {
        if (instance == null)
            instance = new Enemys_Behaviour();
        return instance;
    }

    private int speed_angle, aux_speed;
    private float speed_horizontal;

    public Enemys_Behaviour()
    {
        this.speed_angle = 1;
        this.speed_horizontal = .15f;
        this.aux_speed = 1;
    }

    public void behaviours(GameObject g, string nivel, int angle)
    {
        if (nivel == "math")
        {
            switch (g.gameObject.name.Substring(2))
            {
                case "0":
                    //this.move_horizontal(g.gameObject, 2.15f);
                    break;
                case "3":
                    this.falling_enemys(g, angle);
                    break;
            }
        }

        if (nivel == "physic")
        {
            switch (g.gameObject.name.Substring(2))
            {
                case "0":
                case "1":
                    break;
                case "2":
                    g.GetComponent<Enemys>().setHorizontal(true);
                    break;
            }
        }


        if (nivel == "chemistry")
        {
            switch (g.gameObject.name.Substring(2))
            {
                case "0":
                case "1":
                case "2":
                    g.GetComponent<Enemys>().setHorizontal(true);
                    break;
                case "3":
                    g.GetComponent<Enemys>().setHorizontal(false);
                    break;

            }
        }
    }

    void falling_enemys(GameObject g, int angle)
    {
        if (g.transform.position.x < -2.45f)
            g.transform.position = new Vector2(-2.45f, g.transform.position.y);
        else if (g.transform.position.x > 2.45f)
            g.transform.position = new Vector2(2.45f, g.transform.position.y);

        angle += 1;
        g.transform.Translate(0, -1f * (angle * .05f) * (Time.deltaTime * 10), 0);
    }

}
