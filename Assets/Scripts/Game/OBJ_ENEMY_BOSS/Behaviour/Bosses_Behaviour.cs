﻿using UnityEngine;
using System.Collections;

public class Bosses_Behaviour
{
    private static Bosses_Behaviour instance = null;

    public static Bosses_Behaviour getInstance()
    {
        if (instance == null)
            instance = new Bosses_Behaviour();

        return instance;
    }

    private float speed_horizontal, sec, alpha;
    private int r;
    private bool gust;

    public Bosses_Behaviour()
    {
        this.speed_horizontal = .25f;
        this.sec = 0;
        this.r = 0;
        this.alpha = 1;
        this.gust = false;
    }

    private void behaviour_manager(GameObject g, int id)
    {
        switch (id)
        {
            case 0:
                this.pitagoras(g);
                break;
            case 1:
                this.galileu(g);
                break;
            case 2:
                this.jackson(g);
                break;
        }
    }

    public void general_behaviour(GameObject g, int id)
    {
        if (g.transform.position.y > 3.5f)
            g.transform.Translate(0, -.25f * (Time.deltaTime * 10), 0);
        else
            this.behaviour_manager(g, id);
    }

    private void pitagoras(GameObject g)
    {
        this.gustMode();
        this.move_default(g, 2.15f);
    }

    private void galileu(GameObject g)
    {
        this.trapMode(g,0);
        this.move_default(g, 2.2f);
    }

    private void jackson(GameObject g)
    {
        this.move_default(g, 2.15f);
        this.trapMode(g,2);
    }

    private void move_default(GameObject g, float limit)
    {
        g.transform.Translate(this.speed_horizontal * (Time.deltaTime * 10), 0, 0);

        if (g.transform.position.x <= -limit || g.transform.position.x >= limit)
            if (g.transform.position.x < 0 && this.speed_horizontal < 0 || g.transform.position.x > 0 && this.speed_horizontal > 0)
                this.speed_horizontal *= -1;
    }

    private void gustMode()
    {
        if (this.r != 2)
        {
            this.sec = 0;
            this.r = Random.Range(0, 200);
            GameObject.FindObjectOfType<Bosses>().set_shot(.5f);
        }
        else
        {
            this.sec += (.01f * (Time.deltaTime * 100));
            this.setGust(true); GameObject.FindObjectOfType<Bosses>().set_shot(.1f);
            if (this.sec >= 1) { this.setGust(false); this.r = 0; }
        }
    }
    private void trapMode(GameObject g, int aux)
    {
        if (this.r == 2)
        {
            if (this.sec >= 2+aux)
            {
                this.setInvisible(g, true);
                GameObject.FindObjectOfType<Bosses>().set_canshot(true);
                GameObject.FindObjectOfType<Bosses>().set_shot(.05f);

                if (this.sec >= 3.5f+aux)
                    this.r = 0;
            }

            else
            {
                GameObject.FindObjectOfType<Bosses>().set_canshot(false);
                this.setInvisible(g, false);
            }
            this.sec += (.01f * (Time.deltaTime * 100));

        }

        else if (this.r != 2)
        {
            this.sec = 0;
            this.setInvisible(g, true);
            GameObject.FindObjectOfType<Bosses>().set_canshot(true);
            GameObject.FindObjectOfType<Bosses>().set_shot(.5f);
            this.r = Random.Range(0, 300);
        }
    }

    private void setInvisible(GameObject g, bool more)
    {
        this.alpha = (more) ? ((this.alpha < 1) ? this.alpha += .025f * (Time.deltaTime * 100) : 1) : ((this.alpha > 0) ? this.alpha -= .025f * (Time.deltaTime * 100) : 0);
        g.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, this.alpha);

        this.disableComponents(g, more);
    }

    private void disableComponents(GameObject g, bool enable) { g.GetComponent<PolygonCollider2D>().enabled = enable; }
    public void setGust(bool n) { this.gust = n; }
    public bool getGust() { return this.gust; }
}
