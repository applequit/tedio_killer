﻿using UnityEngine;
using System.Collections.Generic;

public class Identity
{
    private static Identity instance = null;

    public static Identity getInstance()
    {
        if (instance == null)
            instance = new Identity();

        return instance;
    }

    private string[] typesSkin = new string[]{
        "rabiskum", "esboçoria", "illustrator"
    };

    public string Skin;

    public List<bool> unlocked = new List<bool>();

    private int n_nave, select_index;

    public Identity()
    {
        //this.select_index = 0; //Quando selecionar a nave
        this.unlocked.Add(true); this.unlocked.Add(true); this.unlocked.Add(true);
    }

    public void managerControl() { this.setNave(this.unlocked.Count); this.Skin = this.typesSkin[this.getSelect()]; }

    public void lockedControl(GameObject g)
    {
        for (int i = 0; i < this.n_nave; i++)
        {
            if (this.unlocked[i] && g.name.EndsWith("" + i))
                g.tag = "unlocked";
        }
    }
    public int getDamage()
    {
        int d = 0;
        switch (this.getSelect())
        {
            case 0://rabiskum
                d = 1;
                break;
            case 1://esboçoria
                d = 1;
                break;
            case 2://illustrator
                d = 2;
                break;
        }
        return d;
    }

    public int getResistance()
    {
        int r = 0;
        switch (this.getSelect())
        {
            case 0://rabiskum
                r = 2;
                break;
            case 1://esboçoria
                r = 4;
                break;
            case 2://illustrator
                r = 5;
                break;
        }
        return r;
    }

    public float getShootingRate()
    {
        float rate = 0;
        switch (this.getSelect())
        {
            case 0:
                rate = 1;
                break;
            case 1:
                rate = .85f;
                break;
            case 2:
                rate = .7f;
                break;
        }
        return rate;
    }

    public int getSelect() { return select_index; }
    public void setSelect(int index) { this.select_index = index; LoadSaveController.SaveData("nave");}

    public int getNave() { return n_nave; }
    public void setNave(int N) { this.n_nave = N; }

    public string getSkin() { return Skin; }
    public void setSkin(string Skin) { this.Skin = Skin; }

}
