﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour
{
    Pause_Manager pauseManager;
    Textures texture;

    Vector2 position, change_vector;

    private Sprite myImg = Resources.Load("Images/Game/Main/Comum/" + "tiro1", typeof(Sprite)) as Sprite;

    int r;

    void Start()
    {
        //this.myImg = Resources.Load("Images/Game/Main/Comum/" + "tiro", typeof(Sprite)) as Sprite;

        gameObject.AddComponent<SpriteRenderer>();
        gameObject.AddComponent<BoxCollider2D>();
        this.gameObject.AddComponent<Rigidbody2D>();

        this.pauseManager = Pause_Manager.getInstance();
        this.texture = Textures.getInstance();

        gameObject.GetComponent<SpriteRenderer>().sprite = this.getIMG();
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 2;

        this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;

        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        //this.gameObject.GetComponent<Rigidbody2D>().fixedAngle = true;

        this.r = Random.Range(1, -1);
        this.change_vector = (this.r > 0) ? new Vector2(1f, 1f) : new Vector2(-1f, -1f);

        this.transform.position = this.getPosition();
        this.transform.localScale = new Vector2(.8f, .8f);
    }

    void Update()
    {
        if(!this.pauseManager.getBool() && !GameObject.FindObjectOfType<Pause_Counter>().getStarted())
            this.move_default((int.Parse(gameObject.name.Substring(4)) == 0) ? 1 : -1, int.Parse(this.gameObject.name.Substring(4)));
    }

    public void move_default(int math, int id)
    {
        transform.Translate(0, math * (Time.deltaTime * 10), 0);

        if (this.transform.position.y > 5.6F || this.transform.position.y < -5.6f)
            Destroy(this.gameObject);

        if (id == 2)
        {
            transform.Translate(this.change_vector.x * (Time.deltaTime * 10), -this.change_vector.y * (Time.deltaTime * 1f), 0);

            if (this.transform.position.x > 2.91f && this.change_vector.x > 0 || this.transform.position.x < -2.91f && this.change_vector.x < 0)
                this.change_vector.x *= -1f;
        }
    }

    public void setPosition(Vector2 pos) { this.position = pos; }
    private Vector2 getPosition() { return this.position; }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name.StartsWith("obj")||c.name.Equals("shot0") ){
            Destroy(this.gameObject);

            if (c.name.Equals("shot0"))
                Destroy(c.gameObject);
        }
    }

    public void setIMG(string s) { this.myImg = Resources.Load("Images/Game/Main/Comum/" + s, typeof(Sprite)) as Sprite; ; }
    public Sprite getIMG() { return this.myImg; }
}
