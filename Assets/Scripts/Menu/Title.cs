﻿using UnityEngine;
using System.Collections;

public class Title : MonoBehaviour
{
    Textures texture;

    Vector2[] position = new Vector2[]{
        new Vector2(0, 2.83f),//main
        new Vector2(.1f, -11.45f),//nave
        new Vector2(0,-22.35f),//credits
        //new Vector2(0,-33),//extra
        //new Vector2(0, -44.1F),//extra2
        //new Vector2(0, -54f),//extra3
        new Vector2(6.12f, 0),//config
        new Vector2(0,11f),//select
        //new Vector2(-2,-1.3f)//indicador jogar
    };

    void Start()
    {
        gameObject.AddComponent<SpriteRenderer>();

        this.texture = Textures.getInstance();
        this.gameObject.GetComponent<SpriteRenderer>().sprite = texture.D_Title[int.Parse(gameObject.name.Substring(5))];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 12;

        this.gameObject.transform.position = this.position[int.Parse(gameObject.name.Substring(5))];
    }
}
