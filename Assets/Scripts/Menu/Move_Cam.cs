﻿using UnityEngine;
using System.Collections;

public class Move_Cam : MonoBehaviour
{
    Cam_Manager caManager;

    void Start()
    {
        this.caManager = Cam_Manager.getInstance();
    }

    void LateUpdate()
    {
        this.caManager.setMove(this.gameObject);
    }
}
