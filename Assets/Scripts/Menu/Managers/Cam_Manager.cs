﻿using UnityEngine;
using System.Collections;

public class Cam_Manager
{
    private static Cam_Manager instance = null;

    public static Cam_Manager getInstance()
    {
        if (instance == null)
            instance = new Cam_Manager();
        return instance;
    }

    private int ID;
    private float speed, speed_aux, speed_count;

    float[] position_down = new float[]{
            -11,-22, -33
        };

    float[] position_up = new float[]{
            0,11
        };

    public Cam_Manager()
    {
        this.ID = -2;
        this.speed_count = 0;
        this.speed = 0;
        this.speed_aux = 0;
    }

    public void setMove(GameObject g)
    {
        /*  ID = 1,2,3,4, -5 ~ Down 
            ID = -2, -1 -6~ Up 
            ID = -3 ~ Right
            ID = -4 ~ Left  */

        //Down
        if ((this.getID() - 2) >= 0)
            if (g.transform.position.y > position_down[this.getID() - 2])
                this.setConfig(-1, g);
            else if (g.transform.position.y < position_down[this.getID() - 2])
                this.resetConfig(position_down[this.getID() - 2], g);

        //Down ~ No Conflict
        if (this.getID() == -5)
            if (g.transform.position.y > 0)
                g.transform.Translate(0, -.1f, 0);
            else if (g.transform.position.y < 0)
                this.resetConfig(0, g);

        //Up
        if (this.getID() == -1 || this.getID() == -2)
            if (g.transform.position.y < this.position_up[this.getID() + 2])
                this.setConfig(1, g);
            else if (g.transform.position.y > 0)
                this.resetConfig(this.position_up[this.getID() + 2], g);

        //Up ~ No Limit
        if(this.getID() == -6)
            this.setConfig(1, g);

        //Left
        if (this.getID() == -4)
            if (g.transform.position.x < 6.1f)
                this.setConfig_vertical(.1f, g);
            else if (g.transform.position.x > 6.1f)
                this.resetConfig_vertical(6.1f, g);

        //Right
        if (this.getID() == -3)
            if (g.transform.position.x > 0)
                this.setConfig_vertical(-.1f, g);
            else if (g.transform.position.x < 0)
                this.resetConfig_vertical(0, g);
    }

    private void setConfig_vertical(float var, GameObject g)
    {
        Button_Back.BStop = false;
        g.transform.Translate(var, 0, 0);
    }

    private void resetConfig_vertical(float var, GameObject g)
    {
        Button_Back.BStop = true;
        g.transform.position = new Vector3(var, g.transform.position.y, g.transform.position.z);
    }

    private void setConfig(int math, GameObject g)
    {
        Button_Back.BStop = false;
        this.setSpeed();
        g.transform.position = new Vector3(g.transform.position.x, math * this.speed, g.transform.position.z);
    }

    private void resetConfig(float position, GameObject g)
    {
        Button_Back.BStop = true;
        this.speed_count *= 0;
        this.speed_aux *= 0;
        this.speed = position;
        g.transform.position = new Vector3(g.transform.position.x, position, g.transform.position.z);
    }

    private void setSpeed()
    {
        this.speed_count += 5;
        this.speed_aux += (int)(this.speed_count) * .03f;
        this.speed += (1 + (this.speed_aux * .1f)) * .025f;
    }

    public void setID(int ID) { this.ID = ID; }
    public int getID() { return this.ID; }
}
