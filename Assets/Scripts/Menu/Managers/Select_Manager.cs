﻿using UnityEngine;
using System.Collections;

public class Select_Manager
{

    private static Select_Manager instance = null;

    public static Select_Manager getInstance()
    {
        if (instance == null)
            instance = new Select_Manager();
        return instance;
    }

    Wave_Manager wManager;

    private int nivel, liberty, real_nivel; //math, physic, chemistry

    public Select_Manager()
    {
        this.wManager = Wave_Manager.getInstance();
        this.liberty = 1;
        //Pega o valor mais recente armazenado no save
        if (LoadSaveController.instance != null)
            this.nivel = LoadSaveController.instance.nivelUnlock;
        else
            this.nivel = 0;
        //this.nivel = 4;
        this.real_nivel = 0;
    }

    public void Update() { this.manager_nivel(); this.liberty = this.wManager.getUnlock(this.liberty); this.real_nivel = (this.nivel > this.real_nivel) ? this.nivel : this.real_nivel; }

    private void manager_nivel(){
        switch (this.wManager.getNivel())
        {
            case "math":
                this.nivel = (this.wManager.getWave() >= 1) ? 2 : this.nivel;
                break;
            case "physic":
                this.nivel = 3;
                break;
            case "chemistry":
                this.nivel = 4;
                break;
        }
    }

    public void set_config(GameObject g){//Aqui que a mágica acontece
        switch (int.Parse(g.gameObject.name.Substring(7)))//Pego o nome dos botões e corto as letras, deixando somente o numero final, que são os indices
        {
            case 0://indice 0 é o botão ABC
                this.wManager.setNivel("math");
                this.wManager.setWave(0);
                break;
            case 1://ind. 1 é o botão de Math
                this.wManager.setNivel("math");
                this.wManager.setWave(1);
                break;
            case 2://ind. 2 é o botão de Fisica
                this.wManager.setNivel("physic");
                this.wManager.setWave(1);
                break;
            case 3: //ind. 3 é o botão de Quimica
                this.wManager.setNivel("chemistry");
                this.wManager.setWave(1);
                break;
        }
    }

	public void setNivel(int nivel) { this.nivel = nivel; LoadSaveController.SaveData("fase_unlock");}
	public int getFakeNivel() { return this.nivel; }
    public int getNivel() { return this.real_nivel; }
    public int getLiberty() { return this.liberty; }
}
