﻿using UnityEngine;
using System.Collections;

public class Buttons_Select : MonoBehaviour
{
    Analytics_Manager analyticsManager;
    Wave_Manager wManager;
    Cam_Manager cManager;
    Select_Manager sManager;
    Textures texture;

    public int id;

    GameObject g;

    Vector2[] position = new Vector2[]{
        new Vector2(1.5f,7.3f),
        new Vector2(1.5f,10),
        new Vector2(1.5f,12),        
        new Vector2(1.5f,14.3f),
    };

    void Start()
    {
        this.id = 0;

        this.analyticsManager = Analytics_Manager.getInstance();
        this.wManager = Wave_Manager.getInstance();
        this.cManager = Cam_Manager.getInstance();
        this.sManager = Select_Manager.getInstance();
        this.texture = Textures.getInstance();

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<BoxCollider2D>();

        this.setLiberty(this.id);
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 12;

        this.transform.position = this.position[int.Parse(this.gameObject.name.Substring(7))];
    }

    void Update()
    {
        this.setLiberty(this.id);
        for (int i = 0; i < this.sManager.getNivel(); i++) 
            if (int.Parse(this.gameObject.name.Substring(7)) == i)
                this.id = 1;

        this.sManager.Update();
    }

    void OnMouseDown()
    {
        if (this.id == 1)
            this.transform.localScale = new Vector2(1.3f, 1.3f);
    }

    void OnMouseUp()
    {
        if (this.id == 1)
        {
            this.transform.localScale = Vector2.one;
            this.cManager.setID(-6);

            this.analyticsManager.sendScene(1);
            g = new GameObject("fadeOUT", typeof(Fade_Out));//Aqui inicia o fadeOut e quando termina o fadeOut, eu mando ir pro Application.LoadLevel, ae as configs feitas no "Select_Manager" já estarão feitas

            this.sManager.set_config(this.gameObject);//Pega o nome do botão setado e envia pra função set_config do cód. "Select_Manager"
        }
        
    }

    private void setLiberty(int id)
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.Button_Select[id];
        this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.texture.Button_Select[id].rect.width / 100,
                                                                            this.texture.Button_Select[id].rect.height / 100);
    }
}
