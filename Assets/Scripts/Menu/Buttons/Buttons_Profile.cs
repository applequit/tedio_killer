﻿using UnityEngine;
using System.Collections;

public class Buttons_Profile : MonoBehaviour {
    private Vector2[] position = new Vector2[]{
        new Vector2(2.6f, 5f),
        new Vector2(-2.62f, -4.95f),
        new Vector2(-2.6f,-3.8f)
    };

    GameObject g;

    Textures textures;
    FB_Manager fbmanager;
    public bool isClicked;

    void Start() {
        this.isClicked = false;
        this.fbmanager = new FB_Manager();
        this.textures = Textures.getInstance();

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<BoxCollider2D>();

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.textures.SocialNetwork[int.Parse(this.gameObject.name.Substring(2))];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 13;

        this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.textures.SocialNetwork[int.Parse(this.gameObject.name.Substring(2))].rect.width / 100,
                                                                        this.textures.SocialNetwork[int.Parse(this.gameObject.name.Substring(2))].rect.height / 100);

        this.transform.position = this.position[int.Parse(this.gameObject.name.Substring(2))];
    }

    void Update() {
        if(GameObject.FindObjectOfType<MainProfile>().getCNX_FB())
            this.addFacebook();
    }

    void OnMouseDown() {
        this.transform.localScale = new Vector2(1.1f, 1.1f);
    }

    void OnMouseUp() {
        this.transform.localScale = Vector2.one;

        switch(int.Parse(this.gameObject.name.Substring(2))) {
            case 0:
                this.g = new GameObject("FadeOut", typeof(FadeOut_Net));
                break;
            case 1:
                GameObject.FindObjectOfType<MainProfile>().setCNX_FB(true);
                break;
            case 2:
                //GameObject.FindObjectOfType<CamMove_Profile>().setMove(true);
                break;
        }
    }

    void addFacebook() {
        if(GameObject.FindObjectOfType<FB_Manager>() == null)
            this.g = new GameObject("FB Connect", typeof(FB_Manager));
    }
}
