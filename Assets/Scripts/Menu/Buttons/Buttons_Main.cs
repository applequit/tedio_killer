﻿using UnityEngine;
using System.Collections;

public class Buttons_Main : MonoBehaviour {
    Analytics_Manager analyticsManager;
    Cam_Manager caManager;
    Textures texture;

    GameObject g;
    Vector2[] position = new Vector2[]{
        new Vector2(0, -2.5f),//recorde
        new Vector2(0, -0.5f),//campanha
        new Vector2(0, -3.5f),//nave
        new Vector2(0, -4.5f),//credito
        new Vector2(0,-1.5f),//valentoes
        new Vector2(2.6f, -5),//config
        new Vector2(-2.63f, -5)//profile
    };

    bool canRotate;

    void Start() {
        this.canRotate = false;
        gameObject.AddComponent<SpriteRenderer>();
        gameObject.AddComponent<BoxCollider2D>();

        this.analyticsManager = Analytics_Manager.getInstance();
        this.caManager = Cam_Manager.getInstance();
        this.texture = Textures.getInstance();

        this.caManager.setID(-2);

        this.gameObject.GetComponent<SpriteRenderer>().sprite = texture.D_Button[int.Parse(this.gameObject.name.Substring(1))];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 11;

        this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(texture.D_Button[int.Parse(this.gameObject.name.Substring(1))].rect.width / 100,
                                                                         texture.D_Button[int.Parse(this.gameObject.name.Substring(1))].rect.height / 100);

        this.gameObject.transform.position = this.position[int.Parse(this.gameObject.name.Substring(1))];

        if(this.gameObject.name.Substring(1) == "6")
            this.gameObject.GetComponent<SpriteRenderer>().material.color = Color.black;

    }

    void Update() {
        if(this.canRotate)
            transform.Rotate(0, 0, -Time.deltaTime * 100);

        if(Button_Back.isClicked) {
            this.canRotate = false;
            this.transform.eulerAngles = Vector3.zero;
        }
    }

    void OnMouseDown() {
        //Sound_Manager.getInstance().setID(1);
        if(Button_Back.BStop) {
            if(!GameObject.Find("fadeOUT")) {
                if(this.gameObject.name.Substring(1) != "6")
                    this.transform.localScale = new Vector2(1.3f, 1.3f);
                else
                    this.gameObject.GetComponent<SpriteRenderer>().material.color = Color.white;
            }
        }
    }

    void OnMouseUp() {

        if(Button_Back.BStop) {
            if(!GameObject.Find("fadeOUT")) {
                this.transform.localScale = Vector2.one;

                if(this.gameObject.name.Substring(1) != "6") {
                    if(this.gameObject.name.EndsWith("5")) {
                        Button_Back.isClicked = false;
                        this.canRotate = true;
                        this.caManager.setID(-4);
                    }

                    if(int.Parse(this.gameObject.name.Substring(1)) > 1 && !this.gameObject.name.EndsWith("5") && !this.gameObject.name.EndsWith("4"))
                        this.caManager.setID(int.Parse(this.gameObject.name.Substring(1)));//DOWN

                    if(int.Parse(this.gameObject.name.Substring(1)) == 1 || int.Parse(this.gameObject.name.Substring(1)) == 4) {
                        this.caManager.setID(-1);
                        Create.mode = (int.Parse(this.gameObject.name.Substring(1)) != 4) ? "normal" : "boss";
                    }

                    this.analyticsManager.sendScene(this.caManager.getID());
                }
                else {//profile                
                    this.gameObject.GetComponent<SpriteRenderer>().material.color = Color.black;
                    this.g = new GameObject("FadeOut", typeof(FadeOut_Net));
                }
            }
        }
    }
}
