﻿using UnityEngine;
using System.Collections;

public class Button_Back : MonoBehaviour
{
    public bool liberty;
    public static bool BStop, isClicked;

    Analytics_Manager analyticsManager;
    Cam_Manager caManager;
    Textures texture;

    Vector2[] position = new Vector2[]{
        new Vector2(-2.4f,-15.7f),
        new Vector2(-2.4f,-26.7f),
        new Vector2(-2.4f,-37.7f),
        new Vector2(3.7F, -4.65f),
        new Vector2(-2.4f, 6.35f),
    };

    void Awake()
    {
        this.liberty = true;
        BStop = false;
        isClicked = false;
    }

    void Start()
    {
        gameObject.AddComponent<SpriteRenderer>();
        gameObject.AddComponent<BoxCollider2D>();

        this.analyticsManager = Analytics_Manager.getInstance();
        this.caManager = Cam_Manager.getInstance();
        this.texture = Textures.getInstance();

        this.gameObject.GetComponent<SpriteRenderer>().sprite = texture.B_Back;
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 11;

        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        this.gameObject.GetComponent<BoxCollider2D>().size = this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(texture.B_Back.rect.width / 100,
                                                                         texture.B_Back.rect.height / 100);

        this.gameObject.transform.position = this.position[int.Parse(this.gameObject.name.Substring(4))];
    }

    void Update()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = BStop;

        if (int.Parse(this.gameObject.name.Substring(4)) == 3)
            this.gameObject.GetComponent<SpriteRenderer>().enabled = BStop;
    }

    void OnMouseDown()
    {
        transform.localScale = new Vector2(1.3f, 1.3f);
        isClicked = false;
    }

    void OnMouseUp()
    {
        transform.localScale = Vector2.one;

        switch (int.Parse(gameObject.name.Substring(4)))
        {
            case 0:
            case 1:
            case 2:
                this.caManager.setID(-2); //UP
                break;
            case 3:
                this.caManager.setID(-3); //RIGHT
                break;
            case 4:
                this.caManager.setID(-5); //DOWN
                break;
        }
        isClicked = true;

        this.analyticsManager.sendScene(0);

        this.liberty = true;
    }
}
