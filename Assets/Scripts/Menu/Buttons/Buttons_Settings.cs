﻿using UnityEngine;
using System.Collections;

public class Buttons_Settings : MonoBehaviour
{
    int id;

    Textures texture;
    Key_Manager kManager;
    Config_Select cSelect;
    Sound_Manager sManager;

    GameObject g;

    Vector2[] position = new Vector2[]{
        new Vector2(4f, -1.7f),
        new Vector2(8, -1.55f),
        new Vector2(8f, 1.6f),
        new Vector2(4.5f, 1.6f),
    };

    float[] correction = new float[]{
        .46f, .37f
    };

    // Use this for initialization
    void Start()
    {
        this.id = 0;
        this.cSelect = new Config_Select();

        this.sManager = Sound_Manager.getInstance();
        this.texture = Textures.getInstance();
        this.kManager = Key_Manager.getInstance();

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<BoxCollider2D>();

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.Button_Config[int.Parse(this.gameObject.name.Substring(9))];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 11;

        this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.texture.Button_Config[int.Parse(this.gameObject.name.Substring(9))].rect.width / 100,
                                                                        this.texture.Button_Config[int.Parse(this.gameObject.name.Substring(9))].rect.height / 100);

        this.gameObject.transform.position = this.position[int.Parse(this.gameObject.name.Substring(9))];

        if(gameObject.name.Substring(9) == "3")
            gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.Button_Config[int.Parse(this.gameObject.name.Substring(9)) + ((this.sManager.getID()==1)?2:0)];
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
        if (Button_Back.BStop)
        {
            transform.localScale = new Vector2(1.3f, 1.3f);
        }
    }


    void OnMouseUp()
    {
        if (Button_Back.BStop)
        {
            transform.localScale = Vector2.one;

            if (int.Parse(this.gameObject.name.Substring(9)) < 2)
            {
                Destroy(GameObject.Find("slct"));

                this.cSelect.setSelect(this.g, new Vector2(this.transform.position.x + this.correction[int.Parse(this.gameObject.name.Substring(9))], -1.1f), "slct");

                this.kManager.setKey(int.Parse(this.gameObject.name.Substring(9)));
            }

            else
            {
                this.id = (this.id == 0) ? 2 : 0;
                for (int i = 0; i < 2; i++)
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.Button_Config[int.Parse(this.gameObject.name.Substring(9)) + this.id];

                if (gameObject.name.Substring(9) == "3")
                    this.sManager.setID((this.id == 2)? 1:0);
            }
        }
    }


}
