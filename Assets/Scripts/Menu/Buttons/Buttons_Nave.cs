﻿using UnityEngine;
using System.Collections;

public class Buttons_Nave : MonoBehaviour
{
    GameObject g;
    string tag_skin;
    int end_index;
    int[] id;

    Config_Select cSelect;
    Naves_Manager nManager;
    Textures texture;
    Identity identity;

    Sprite[] all_textures = new Sprite[]{
                Resources.Load("Images/Menu/Nave/icon"+1, typeof(Sprite)) as Sprite,
                Resources.Load("Images/Game/Main/Player/" + "rabiskum" + "/nave" + 1, typeof(Sprite)) as Sprite,
                Resources.Load("Images/Game/Main/Player/" + "esboçoria" + "/nave" + 1, typeof(Sprite)) as Sprite,
                Resources.Load("Images/Game/Main/Player/" + "illustrator" + "/nave" + 1, typeof(Sprite)) as Sprite,

            };

    void Start()
    {
        this.id = new int[3];

        if (this.gameObject.name.StartsWith("select"))
            this.end_index = int.Parse(gameObject.name.Substring(6))+1;

        for (int i = 0; i < this.id.Length; i++ )
            this.id[i] = 1;

        this.cSelect = new Config_Select();
        this.nManager = Naves_Manager.getInstance();
        this.identity = Identity.getInstance();
        this.texture = Textures.getInstance();

        this.StartCoroutine(this.anim_select());

        gameObject.AddComponent<SpriteRenderer>();
        gameObject.AddComponent<BoxCollider2D>();

        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 11;
        //this.gameObject.transform.position = this.position[int.Parse(this.gameObject.name.Substring(1))];
    }

    void Update()
    {
        transform.position = new Vector2(0, -(int.Parse(gameObject.name.Substring(6)) * 2.5f + 10));

        this.identity.managerControl();
        this.identity.lockedControl(this.gameObject);

        if (this.gameObject.tag == "unlocked")
            this.setSprite(this.end_index);
        else
            this.setSprite(0);
    }

    void OnMouseUp()
    {
        if (Button_Back.BStop)
        {
            if (this.gameObject.tag.Equals("unlocked"))
            {
                Destroy(GameObject.Find("slc"));
                this.cSelect.setSelect(this.g, new Vector2(this.transform.position.x + .35f, this.transform.position.y + .2f), "slc");

                this.identity.setSelect(this.end_index - 1);
            }
        }
    }

    private Sprite getSprite(Sprite spr) { return spr; }

    private void setSprite(int indice)
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.all_textures[indice];

        this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.all_textures[indice].rect.width / 100,
                                                                         this.all_textures[indice].rect.height / 100);
    }

    IEnumerator anim_select()
    {
        while (true)
        {
            yield return new WaitForSeconds(.09f);

            this.id[this.identity.getSelect()]++;
            if (this.id[this.identity.getSelect()] > 3)
                this.id[this.identity.getSelect()] = 1;

            all_textures = new Sprite[]{
                Resources.Load("Images/Menu/Nave/icon"+1, typeof(Sprite)) as Sprite,
                Resources.Load("Images/Game/Main/Player/" + "rabiskum" + "/nave" + id[0], typeof(Sprite)) as Sprite,
                Resources.Load("Images/Game/Main/Player/" + "esboçoria" + "/nave" + id[1], typeof(Sprite)) as Sprite,
                Resources.Load("Images/Game/Main/Player/" + "illustrator" + "/nave" + id[2], typeof(Sprite)) as Sprite,

            };
        }
    }

}
