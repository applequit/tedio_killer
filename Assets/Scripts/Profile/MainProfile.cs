﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainProfile : MonoBehaviour {

    Analytics_Manager analytics;

    GUIStyle user_name;

    protected string text_user = "";

    private bool check_cnx;

    void Awake() {
    }

    void Start() {
        this.check_cnx = false;
        this.analytics = Analytics_Manager.getInstance();

        this.user_name = new GUIStyle();
        this.user_name.font = (Font)Resources.Load("Fonts/BEFRISKY");
        this.user_name.alignment = TextAnchor.MiddleCenter;

    }

    void Update() {
        GameObject.Find("Hello").GetComponent<Text>().text = "Seja Bem Vindo(a)" + this.text_user + "!";
    }

    public void setText(string s) { this.text_user = s; }

    public void setCNX_FB(bool cnx) { this.check_cnx = cnx; }
    public bool getCNX_FB() { return this.check_cnx; }
}
