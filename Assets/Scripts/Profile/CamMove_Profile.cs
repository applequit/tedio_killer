﻿using UnityEngine;
using System.Collections;

public class CamMove_Profile : MonoBehaviour {

    private bool canMove;
    private float speed;


    void Start() {
        this.speed = 1;
        this.canMove = false;
    }

    void Update() {

    }

    void LateUpdate() {

        if(this.canMove) {
            if(this.transform.position.x > -6.1f) {
                this.speed += 1;
                this.transform.Translate(-this.speed * Time.deltaTime, 0, 0);
            }
            else
                this.speed = 1;
        }
    }

    public void setMove(bool b) { this.canMove = b; }
    public bool getMove() { return this.canMove; }
}
