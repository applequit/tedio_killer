﻿using UnityEngine;
using System.Collections;

public class Config_Select
{

    public Config_Select()
    {

    }

    public void setSelect(GameObject g, Vector2 position, string name)
    {
        g = new GameObject(name, typeof(Point_Select));
        g.GetComponent<Point_Select>().transform.position = position;

        switch (Application.loadedLevelName)
        {
            case "Game":
                g.transform.parent = GameObject.Find("Pause").transform;
                break;
        }

        //g.GetComponent<Point_Select>().transform.eulerAngles = (b) ? new Vector3(0, 0, -30) : Vector3.zero;
    }
}
