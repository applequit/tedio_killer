﻿using UnityEngine;
using System.Collections;

public class Point_Select : MonoBehaviour
{

    Textures texture;
    int id;

    void Start()
    {
        this.id = 0;

        this.texture = Textures.getInstance();

        this.StartCoroutine(this.anim_select());

        this.gameObject.AddComponent<SpriteRenderer>();

        switch (Application.loadedLevelName)
        {
            case "Menu":
                this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.p_select[this.id];
                break;
            case "Game":
                this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.g_select[this.id];
                break;
        }
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 12;

        this.transform.localScale = new Vector2(.7f, .7f);

    }

    IEnumerator anim_select()
    {
        while (true)
        {
            yield return new WaitForSeconds(.01f);

            this.id = (this.id > 7) ? 8 : this.id + 1; 
            switch (Application.loadedLevelName)
            {
                case "Menu":
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.p_select[this.id];
                    break;
                case "Game":
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = this.texture.g_select[this.id];
                    break;
            }

            if (this.gameObject.name.Equals("slc") && this.id == 8 || this.gameObject.name.Equals("slct") && this.id == 8)
                Destroy(this.gameObject, .5F);
        }
    }
}
