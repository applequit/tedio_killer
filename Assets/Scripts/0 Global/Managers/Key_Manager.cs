﻿using UnityEngine;
using System.Collections;

public class Key_Manager
{

    private static Key_Manager instance = null;

    public static Key_Manager getInstance()
    {
        if (instance == null)
            instance = new Key_Manager();
        return instance;
    }

    Identity identity;

    public float speed;// = 10.0F;

    private float[] limit = new float[]{
        2.65f, 2.3f, 2.46f
    };

    public int keySelect;
    public bool touchBar;

    public void setKey(int key) { this.keySelect = key; LoadSaveController.SaveData("controle"); }
    public int getKey() { return keySelect; }

    public Key_Manager()
    {
        this.identity = Identity.getInstance();
        this.touchBar = false;
        this.speed = 10.0f;
    }

    public void main_Manager(GameObject g)
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        this.touchBar = false;
        if (hit.collider != null)
        {
            if (hit.collider.name.StartsWith("tb"))
                this.touchBar = true;
        }

        switch (this.getKey())
        {
            case 0:
                this.key_touch(g);
                break;
            case 1:
                this.key_acelerometer(g);
                break;
        }

        this.control_position(g);
    }

    private void control_position(GameObject g)//limites da tela
    {
        if (g.transform.position.x >= this.limit[this.identity.getSelect()])
            g.transform.position = new Vector2(this.limit[this.identity.getSelect()], g.transform.position.y);

        else if (g.transform.position.x <= -this.limit[this.identity.getSelect()])
            g.transform.position = new Vector2(-this.limit[this.identity.getSelect()], g.transform.position.y);
    }

    private void key_acelerometer(GameObject g)
    {
        Vector3 dir = Vector3.zero;

        if (Input.acceleration.x > 0.1f)
            dir.x = .5f;
        else if (Input.acceleration.x < -0.1f)
            dir.x = -.5f;

        dir *= Time.deltaTime * 10;
        g.transform.Translate(dir.x, 0, 0);
    }

    private void key_touch(GameObject g)
    {
        if (Input.GetMouseButton(0))
        {
            if (g.transform.position.x > Camera.main.ScreenToWorldPoint(Input.mousePosition).x + .25f )
                g.transform.Translate(-.5f * (Time.deltaTime * 10), 0, 0);
            else if (g.transform.position.x < Camera.main.ScreenToWorldPoint(Input.mousePosition).x -.25f)
                g.transform.Translate(.5f * (Time.deltaTime * 10), 0, 0);
        }
    }
}
