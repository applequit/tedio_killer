﻿using UnityEngine;
using System.Collections;

public class Naves_Manager {

    private static Naves_Manager instance = null;

    public static Naves_Manager getInstance()
    {
        if (instance == null) instance = new Naves_Manager();

        return instance;
    }

    public Naves_Manager()
    {

    }
}
