﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;

public class FB_Manager : MonoBehaviour {
    private Dictionary<string, string> info_profile = null;

    GameObject profile_picture;

    void Start() {
        this.Init();
    }

    public void addConfig(GameObject g) {
        g.AddComponent<SpriteRenderer>();
        g.transform.position = new Vector2(0, 3.5f);
    }

    public void Init() {
        FB.Init(setInit, OnHideUnity);

        this.profile_picture = new GameObject("field0");
        this.addConfig(this.profile_picture);
    }

    private void setInit() {
        print("Init Done.");

        if(FB.IsLoggedIn)
            print("You're logged.");
        else
            FBLogin();

        this.CheckMenu(FB.IsLoggedIn);
    }

    private void OnHideUnity(bool isShown) {
        Time.timeScale = (isShown) ? 0 : 1;
    }

    public void FBLogin() {
        //FB.Login("user_about_me, user_birthday", AuthCallback);
        FB.Login("email", AuthCallback);
    }

    public void AuthCallback(FBResult result) {
        if(FB.IsLoggedIn)
            Debug.Log("Login worked!");
        else
            Debug.Log("Login fail!");
        this.CheckMenu(FB.IsLoggedIn);
    }

    void CheckMenu(bool isLogged) {
        //foto de perfil
        if(isLogged) {
            FB.API(Util.GetPictureURL("me", 150, 150), Facebook.HttpMethod.GET, CheckProfile);
            FB.API("/me?fields=id,first_name", Facebook.HttpMethod.GET, CheckUser);
        }
    }

    void CheckProfile(FBResult result) {
        if(result.Error != null) {
            Debug.Log("error on the profile picture");

            FB.API(Util.GetPictureURL("me", 150, 150), Facebook.HttpMethod.GET, CheckProfile);
            return;
        }

        Sprite sprite = new Sprite();
        sprite = Sprite.Create(result.Texture, new Rect(0, 0, 150, 150), new Vector2(0.5f, 0.5f));

        this.profile_picture.GetComponent<SpriteRenderer>().sprite = sprite;
    }

    void CheckUser(FBResult result) {
        if(result.Error != null) {
            Debug.Log("error on the user name");

            FB.API("/me?fields=id,first_name", Facebook.HttpMethod.GET, CheckUser);
            return;
        }

        this.info_profile = Util.DeserializeJSONProfile(result.Text);

        GameObject.FindObjectOfType<MainProfile>().setText(", " + this.info_profile["first_name"]);
    }

    public void toShare() {
        FB.Feed(
            linkCaption: "Test TK Fb Integration",
            picture: "https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xaf1/v/t1.0-9/11069861_437270753104407_6953806563460708023_n.jpg?oh=1b801ab3e46eb2d87955bb7031adcaf7&oe=55ED9D76&__gda__=1442309319_d45868a92e6696a2306228d8e76d5ac1",
            linkName: "Test description",
            link: "http://apps.facebook.com/" + FB.AppId + "/?challeng_brag=" + (FB.IsLoggedIn ? FB.UserId : "guest")
            //link: "https://www.facebook.com/tediokiller?fref=ts"
            );

    }

    public void toInvite() {
        FB.AppRequest(
            message: "Venha jogar comigo, amigo(a)!",
            callback: this.CallBack
        );
    }

    private void CallBack(FBResult result) {
        Debug.Log("Result: " + result.Text);
    }
}
