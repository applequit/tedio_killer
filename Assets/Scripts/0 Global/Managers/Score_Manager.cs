﻿using UnityEngine;
using System.Collections;

public class Score_Manager
{
    private static Score_Manager instance = null;

    public static Score_Manager getInstance()
    {
        if (instance == null)
            instance = new Score_Manager();

        return instance;
    }

    private int myScore;

    public void setScore(int score) { this.myScore = score; LoadSaveController.SaveData("score"); }
    public int getScore() { return this.myScore; }

    public Score_Manager()
    {
    }

}
