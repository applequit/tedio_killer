﻿using UnityEngine;
using System.Collections;

public class Sound_Manager
{

    private static Sound_Manager instance = null;

    public static Sound_Manager getInstance()
    {
        if (instance == null)
            instance = new Sound_Manager();
        return instance;
    }

    int id = 0; //somFX

    public void setID(int id) { this.id = id; LoadSaveController.SaveData("somFX"); } //somFX
    public int getID() { return id; } //somFX

    public Sound_Manager()
    {
    }
}
