﻿using UnityEngine;
using System.Collections;

public class Analytics_Manager {

    private static Analytics_Manager instance = null;

    public static Analytics_Manager getInstance() {
        if(instance == null)
            instance = new Analytics_Manager();
        return instance;
    }

    public GoogleAnalyticsV3 googleAnalytics;

    public Analytics_Manager() {
        this.googleAnalytics = GoogleAnalyticsV3.getInstance();
    }

    public void sendScene(int id) {
        switch(id) {
            case -4://Settings
                this.setScreen("Settings");
                break;
            case -2: //Recorde
                this.setScreen("Record Fail");
                break;
            case -1: //Tela de seleção
                this.setScreen("Selection's Screen");
                break;
            case 0://Main Menu
                this.setScreen("Main Menu");
                break;
            case 1://Game
                this.setScreen("Game");
                break;
            case 2: //Nave
                this.setScreen("Nave's Screen");
                break;
            case 3://Creditos
                this.setScreen("Credits");
                break;
            case 4://Extras
                this.setScreen("Extras");
                break;
            case 5://Facebook
                this.setScreen("Facebook Connected");
                break;
            case 99://Pause
                this.setScreen("Pause");
                break;
            case 100://Cutscene Visto
                this.setScreen("Cutscene Ok");
                break;
        }
    }

    private void setScreen(string nome) {
        googleAnalytics.LogScreen(nome);
        googleAnalytics.LogScreen(new AppViewHitBuilder()
            .SetScreenName(nome));
    }
}
