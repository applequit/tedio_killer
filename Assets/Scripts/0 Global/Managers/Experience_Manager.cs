﻿using UnityEngine;
using System.Collections;

public class Experience_Manager
{
    private static Experience_Manager instance = null;

    public static Experience_Manager getInstance()
    {
        if (instance == null)
            instance = new Experience_Manager();
        return instance;
    }

    private int myXP;

    public void setXP(int xp) { this.myXP = xp; LoadSaveController.SaveData("xp");}
    public int getXP() { return this.myXP; }

    public Experience_Manager()
    {
    }
}
