﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class FX_Manger
{
    private static FX_Manger instance = null;

    public static FX_Manger getInstance()
    {
        if (instance == null)
            instance = new FX_Manger();
        return instance;
    }

    private int touch;

    public FX_Manger()
    {
        this.touch = 0;
    }

    public void controlFX()
    {
        if (Input.GetMouseButtonUp(0) && Input.touchCount == 2)
        {
            this.setTouch((this.getTouch() == 1) ? 0 : 1);
            GameObject.Find("Main Camera").GetComponent<VignetteAndChromaticAberration>().enabled = (this.getTouch() == 1) ? true : false;
        }
    }

    public void setTouch(int touch) { this.touch = touch; }
    public int getTouch() { return this.touch; }
}
