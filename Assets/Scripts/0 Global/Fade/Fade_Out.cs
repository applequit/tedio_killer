﻿using UnityEngine;
using System.Collections;

public class Fade_Out : MonoBehaviour
{

    public bool finish, socialnetwork;

    void Start()
    {
        this.socialnetwork = false;

        gameObject.AddComponent<Animator>();
        gameObject.AddComponent<SpriteRenderer>();

        gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load("Images/0 Comum/fade_out", typeof(Sprite)) as Sprite;
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 20;

        gameObject.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)RuntimeAnimatorController.Instantiate(Resources.Load("Animations/0 Comum/fade/fadeOut"));

        gameObject.transform.localScale = new Vector2(1, 20);

    }

    void Update()
    {
        if (this.finish)
        {
            switch (Application.loadedLevelName)
            {
                case "Cutscene":
                    Application.LoadLevel("Menu");
                    break;

                case "Menu":
                    Application.LoadLevel("Game");//aqui o loadlevel pro game
                    break;

                case "Game":
                    Application.LoadLevel("Menu");//aqui o loadlevel pro game
                    break;
            }
        }
    }
}
