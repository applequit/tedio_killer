﻿using UnityEngine;
using System.Collections;

public class Fade_In : MonoBehaviour {

    public bool finish;

	void Start () {
        gameObject.AddComponent<Animator>();
        gameObject.AddComponent<SpriteRenderer>();

        gameObject.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)RuntimeAnimatorController.Instantiate(Resources.Load("Animations/0 Comum/fade/fadeIn"));

        gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load("Images/0 Comum/fade_in", typeof(Sprite)) as Sprite;
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 20;

	}
	
	void Update () {
        if (this.finish)
            Destroy(this.gameObject);
	}
}
