﻿using UnityEngine;
using System.Collections;

public class FadeOut_Net : MonoBehaviour {

    public bool finish;

    Analytics_Manager analytics;

    void Start() {

        gameObject.AddComponent<Animator>();
        gameObject.AddComponent<SpriteRenderer>();

        gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load("Images/0 Comum/fade_out", typeof(Sprite)) as Sprite;
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 20;

        gameObject.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)RuntimeAnimatorController.Instantiate(Resources.Load("Animations/0 Comum/fade/fade_out"));

        gameObject.transform.localScale = new Vector2(1, 20);

    }

    void Update() {
        if(this.finish) {
            switch(Application.loadedLevelName) {
                case "Menu":
                    Application.LoadLevel("MyProfile");
                    break;

                case "MyProfile":
                    Application.LoadLevel("Menu");
                    break;
            }
        }
    }
}
