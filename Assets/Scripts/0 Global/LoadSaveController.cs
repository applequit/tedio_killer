﻿using UnityEngine;
using System.Collections;

//Classe que trabalha com a parte de salvar e carregar os dados no Player.Prefs
public class LoadSaveController : MonoBehaviour {
    
    //Referência estática pra classe
	public static LoadSaveController instance;
	
    //Skin da nave
	public int skinSelecionada = 0;
    //Controle
	public int controleSelecionado = 0;
    //Nível (Fase) liberada
	public int nivelUnlock = 1;
    //XP
    public int xp = 0;
    //Score
    public int score = 0;
    //SomFX
    public int somFX = 0;

    private bool firstTime = true;
	
	void Awake()
	{
		instance = this;
		DontDestroyOnLoad(this);
	}
	
	//Carrega os dados escritos no Player.Prefs
	void Start () {
		LoadData();
	}
	
    //Quando o menu abre pela primeira vez, atualiza os valores para o que estava salvo.
	void OnLevelWasLoaded()
	{
		switch(Application.loadedLevelName)
		{
			case "Menu":
                if (this.firstTime)
                {
                    Identity identity = Identity.getInstance();
                    Key_Manager kmanager = Key_Manager.getInstance();
                    Experience_Manager xpmanager = Experience_Manager.getInstance();
                    Score_Manager scoremanager = Score_Manager.getInstance();
                    Sound_Manager sfxmanager = Sound_Manager.getInstance();
                    if (identity != null)
                        identity.setSelect(skinSelecionada);
                    if (kmanager != null)
                        kmanager.setKey(controleSelecionado);
                    if (xpmanager != null)
                        xpmanager.setXP(xp);
                    if (scoremanager != null)
                        scoremanager.setScore(score);
                    if (sfxmanager != null)
                        sfxmanager.setID(somFX);
                    this.firstTime = false;
                }
				break;
		}
	}
	
    //Carrega os dados do Player.Prefs nas propriedades da classe.
	public static void LoadData()
	{
		if(PlayerPrefs.HasKey("SkinSelecionada"))
			instance.skinSelecionada = PlayerPrefs.GetInt("SkinSelecionada");
		if(PlayerPrefs.HasKey("ControleSelecionado"))
			instance.controleSelecionado = PlayerPrefs.GetInt("ControleSelecionado");
		if(PlayerPrefs.HasKey("NivelUnlock"))
			instance.nivelUnlock = PlayerPrefs.GetInt("NivelUnlock");
        if (PlayerPrefs.HasKey("XP"))
            instance.xp = PlayerPrefs.GetInt("XP");
        if (PlayerPrefs.HasKey("Score"))
            instance.score = PlayerPrefs.GetInt("Score");
        if (PlayerPrefs.HasKey("SomFX"))
            instance.somFX = PlayerPrefs.GetInt("SomFX");
	}
	
    //Salva os dados do tipo passado como parâmetro no Player.Prefs e atualiza as propriedades da classe.
    //Tipos: "nave","controle","fase_unlock"
	public static void SaveData(string type)
	{
		switch(type)
		{
			case "nave":
				Identity identity = Identity.getInstance();
				if(identity != null)
					instance.skinSelecionada = identity.getSelect();
				PlayerPrefs.SetInt("SkinSelecionada",instance.skinSelecionada);
				break;
				
			case "controle":
				Key_Manager kmanager = Key_Manager.getInstance();
				if(kmanager != null)
					instance.controleSelecionado = kmanager.getKey();
				PlayerPrefs.SetInt("ControleSelecionado",instance.controleSelecionado);
				break;
				
			case "fase_unlock":
				Select_Manager smanager = Select_Manager.getInstance();
				if(smanager != null)
					instance.nivelUnlock = smanager.getFakeNivel();
				PlayerPrefs.SetInt("NivelUnlock",instance.nivelUnlock);
				break;

            case "xp":
                Experience_Manager xpmanager = Experience_Manager.getInstance();
                if (xpmanager != null)
                    instance.xp = xpmanager.getXP();
                PlayerPrefs.SetInt("XP", instance.xp);
                break;
            case "score":
                Score_Manager scoremanager = Score_Manager.getInstance();
                if (scoremanager != null)
                    instance.score = scoremanager.getScore();
                PlayerPrefs.SetInt("Score", instance.score);
                break;
            case "somFX":
                Sound_Manager sfxmanager = Sound_Manager.getInstance();
                if (sfxmanager != null)
                    instance.somFX = sfxmanager.getID();
                PlayerPrefs.SetInt("SomFX", instance.somFX);
                break;
		}
		PlayerPrefs.Save ();
	}
}
