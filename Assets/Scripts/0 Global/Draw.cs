﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Draw : MonoBehaviour {
    Analytics_Manager analyticsManager;
    FX_Manger fxManager;
    Pause_Manager pauseManager;
    protected GameObject g;

    void Start() {

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        this.analyticsManager = Analytics_Manager.getInstance();
        this.fxManager = FX_Manger.getInstance();
        this.pauseManager = Pause_Manager.getInstance();

        //if (Application.loadedLevelName != "Cutscene")
        //GameObject.Find("Main Camera").GetComponent<VignetteAndChromaticAberration>().enabled = (this.fxManager.getTouch() == 1) ? true : false;
        this.Draw_Manager(Application.loadedLevelName, this.g);

    }

    // Update is called once per frame
    void Update() {

        if(Application.loadedLevelName != "Game")
            this.pauseManager.setBool(false);

        this.fxManager.controlFX();
    }

    private void Draw_Manager(string name, GameObject g) {
        switch(name) {
            case "Menu":
                this.D_Menu(g);
                this.analyticsManager.sendScene(0);
                break;

            case "Game":
                this.D_Game(g);
                break;

            case "MyProfile":
                this.D_Profile(g);
                break;

        }
    }

    private void D_Menu(GameObject g) {
        g = new GameObject("fadeIn", typeof(Fade_In));

        //Titulo
        for(int i = 0; i < 5; i++)
            g = new GameObject("title" + i, typeof(Title));

        //Botões - Back
        for(int i = 0; i < 5; i++)
            g = new GameObject("back" + i, typeof(Button_Back));

        //Botões - Main
        for(int i = 0; i < 7; i++)
            g = new GameObject("B" + i, typeof(Buttons_Main));

        //Botões - Nave
        for(int i = 0; i < 3; i++) {
            g = new GameObject("select" + i, typeof(Buttons_Nave));
            g.tag = "locked";
        }

        //Botões - Settings
        for(int i = 0; i < 4; i++)
            g = new GameObject("bsettings" + i, typeof(Buttons_Settings));

        //Botões - Select
        for(int i = 0; i < 4; i++)
            g = new GameObject("bselect" + i, typeof(Buttons_Select));
    }

    private void D_Profile(GameObject g) {
        //Botões - Redes Sociais
        for(int i = 0; i < 3; i++)
            g = new GameObject("sn" + i, typeof(Buttons_Profile));
    }

    private void D_Game(GameObject g) {
        g = new GameObject("fadeOUT", typeof(Fade_In));

        g = new GameObject("Player", typeof(Player));

        //Background - Pause
        for(int i = 0; i < 2; i++)
            g = new GameObject("ground" + i, typeof(Background_Pause));

        //Backgrounds
        for(int i = 0; i < 2; i++)
            g = new GameObject("bg" + i, typeof(Background_Game));

        //Top Bar
        for(int i = 1; i < 3; i++) //i<4 botão som
            g = new GameObject("tb" + i, typeof(Interface_Game));

        //Botões - Pause
        for(int i = 0; i < 8; i++)
            g = new GameObject("b" + i, typeof(Buttons_Pause));
    }
}
