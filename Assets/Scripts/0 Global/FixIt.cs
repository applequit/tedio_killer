﻿using UnityEngine;
using System.Collections;

public class FixIt : MonoBehaviour
{
    float unit;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnGUI()
    {
        float targetaspect = 16.0f / 9.0f;
        float windowaspect = (float)Screen.height / (float)Screen.width;
        float scaleheight = windowaspect / targetaspect;


        if ((int)(scaleheight * 100) < 100)
        {
            switch ((int)(scaleheight * 100))
            {
                case 84:
                    this.unit = 1;
                    break;
                case 90:
                    this.unit = 2;
                    break;
                case 93:
                    this.unit = 3;
                    break;
                case 95:
                case 96:
                    this.unit = 4;
                    break;
            }
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), (Texture)Resources.Load("Images/0 Comum/margim" + unit), ScaleMode.StretchToFill, true);
        }
    }
}
