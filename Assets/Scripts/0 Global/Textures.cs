﻿using UnityEngine;
using System.Collections;

public class Textures {
    private static Textures instance = null;

    public static Textures getInstance() {
        if(instance == null)
            instance = new Textures();

        return instance;
    }

    //Menu
    public Sprite[] D_Title, D_Button, Button_Config, Button_Select, p_select;
    public Sprite B_Back;
    public Sprite[] SocialNetwork;

    //Game
    public Sprite Background_Game, shot;
    public Sprite[] player_skin, objects_skin, b_interface, b_pause,
                    title_pause, g_select, enemys_math, enemys_physic,
                    enemys_chemistry, bosses;

    public Textures() {
        this.T_Menu();
        this.T_Game();
    }

    private void T_Menu() {
        this.p_select = new Sprite[9];
        this.Button_Config = new Sprite[6];
        this.D_Title = new Sprite[9];
        this.D_Button = new Sprite[9];
        this.SocialNetwork = new Sprite[3];

        this.B_Back = Resources.Load("Images/0 Comum/botao_voltar", typeof(Sprite)) as Sprite;

        this.D_Title[0] = Resources.Load("Images/Menu/Main/titulo", typeof(Sprite)) as Sprite;
        this.D_Title[1] = Resources.Load("Images/Menu/Nave/titulo", typeof(Sprite)) as Sprite;
        this.D_Title[2] = Resources.Load("Images/Menu/Credits/titulo", typeof(Sprite)) as Sprite;
        this.D_Title[3] = Resources.Load("Images/Menu/Config/titulo", typeof(Sprite)) as Sprite;
        //for (int i = 0; i < 3; i++)
        //this.D_Title[3 + i] = Resources.Load("Images/Menu/Extras/extras" + (i + 1), typeof(Sprite)) as Sprite;
        this.D_Title[4] = Resources.Load("Images/Menu/Main/titulo2", typeof(Sprite)) as Sprite;
        this.D_Title[5] = Resources.Load("Images/Menu/Main/titulo3", typeof(Sprite)) as Sprite;

        this.SocialNetwork[0] = Resources.Load("Images/Profile/x", typeof(Sprite)) as Sprite;
        this.SocialNetwork[1] = Resources.Load("Images/Profile/face_logo", typeof(Sprite)) as Sprite;//facebook
        this.SocialNetwork[2] = Resources.Load("Images/Profile/award", typeof(Sprite)) as Sprite;//award



        for(int i = 0; i < 9; i++)
            this.p_select[i] = Resources.Load("Images/Menu/0 Comum/Select/0" + (i + 1), typeof(Sprite)) as Sprite;


        for(int i = 0; i < 6; i++)
            this.Button_Config[i] = Resources.Load("Images/Menu/Config/b" + i, typeof(Sprite)) as Sprite;//config

        for(int i = -1; i < 7; i++)
            this.D_Button[i + 1] = Resources.Load("Images/Menu/Main/b" + i, typeof(Sprite)) as Sprite;//main
    }

    private void T_Game() {
        this.Button_Select = new Sprite[2];

        this.enemys_math = new Sprite[12]; //a cada 3 é um novo enemy
        this.enemys_chemistry = new Sprite[12]; //a cada 3 é um novo enemy
        this.enemys_physic = new Sprite[9]; //a cada 3 é um novo enemy

        this.bosses = new Sprite[9]; //a cada 3 é um novo boss

        this.g_select = new Sprite[9];
        this.title_pause = new Sprite[2];
        this.objects_skin = new Sprite[4];
        this.b_pause = new Sprite[10];
        this.b_interface = new Sprite[5];

        this.b_interface[0] = Resources.Load("Images/Game/Main/Top bar/icone_linhas", typeof(Sprite)) as Sprite;
        this.b_interface[1] = Resources.Load("Images/Game/Main/Top bar/bar", typeof(Sprite)) as Sprite;
        this.b_interface[2] = Resources.Load("Images/Game/Main/Top bar/botao_pause", typeof(Sprite)) as Sprite;
        this.b_interface[3] = Resources.Load("Images/Game/Main/Top bar/sound1", typeof(Sprite)) as Sprite;
        this.b_interface[4] = Resources.Load("Images/Game/Main/Top bar/sound0", typeof(Sprite)) as Sprite;

        for(int i = 0; i < 2; i++) {
            this.objects_skin[i] = Resources.Load("Images/Game/Main/Objects/erase" + (i + 1), typeof(Sprite)) as Sprite; //Objetos - 1~2
            this.objects_skin[i + 2] = Resources.Load("Images/Game/Main/Objects/pencil" + (i + 1), typeof(Sprite)) as Sprite; //Objetos - 3~4

            this.title_pause[i] = Resources.Load("Images/Game/Pause/title" + i, typeof(Sprite)) as Sprite; //Pause Screen

            this.Button_Select[i] = Resources.Load("Images/Menu/Main/c" + i, typeof(Sprite)) as Sprite; //Select Screen
        }

        for(int i = 0; i < 3; i++) {
            //Math
            this.enemys_math[i] = Resources.Load("Images/Game/Main/Enemy/Math/X/X" + (i + 1), typeof(Sprite)) as Sprite; //X
            this.enemys_math[i + 3] = Resources.Load("Images/Game/Main/Enemy/Math/Y/Y" + (i + 1), typeof(Sprite)) as Sprite; //Y
            this.enemys_math[i + 6] = Resources.Load("Images/Game/Main/Enemy/Math/Pi/Pi" + (i + 1), typeof(Sprite)) as Sprite; //PI
            this.enemys_math[i + 9] = Resources.Load("Images/Game/Main/Enemy/Math/angleL/AReto" + (i + 1), typeof(Sprite)) as Sprite; //Angulo Reto

            //Physic
            this.enemys_physic[i] = Resources.Load("Images/Game/Main/Enemy/Physic/w/W" + (i + 1), typeof(Sprite)) as Sprite; //W
            this.enemys_physic[i + 3] = Resources.Load("Images/Game/Main/Enemy/Physic/omega/Omega" + (i + 1), typeof(Sprite)) as Sprite; //Omega
            this.enemys_physic[i + 6] = Resources.Load("Images/Game/Main/Enemy/Physic/lambda/Lambda" + (i + 1), typeof(Sprite)) as Sprite; //Lambda

            //Chemistry
            this.enemys_chemistry[i] = Resources.Load("Images/Game/Main/Enemy/Chemistry/Benzeno/qui_AnelAromatico" + (i + 1), typeof(Sprite)) as Sprite; //Benzeno
            this.enemys_chemistry[i + 3] = Resources.Load("Images/Game/Main/Enemy/Chemistry/AU/qui_Au" + (i + 1), typeof(Sprite)) as Sprite; //Ouro
            this.enemys_chemistry[i + 6] = Resources.Load("Images/Game/Main/Enemy/Chemistry/C/qui_C" + (i + 1), typeof(Sprite)) as Sprite; //Carbono
            this.enemys_chemistry[i + 9] = Resources.Load("Images/Game/Main/Enemy/Chemistry/Atomo/qui_atomo" + (i + 1), typeof(Sprite)) as Sprite; //Atomo

            //Bosses
            this.bosses[i] = Resources.Load("Images/Game/Main/Enemy/Math/boss/mat" + (i + 1), typeof(Sprite)) as Sprite;//math
            this.bosses[i + 3] = Resources.Load("Images/Game/Main/Enemy/Physic/boss/Fisica" + (i + 1), typeof(Sprite)) as Sprite;//physic
            this.bosses[i + 6] = Resources.Load("Images/Game/Main/Enemy/Chemistry/boss/Quimica" + (i + 1), typeof(Sprite)) as Sprite;//chemistry
        }

        for(int i = 0; i < 9; i++)
            this.g_select[i] = Resources.Load("Images/Game/Pause/Select/0" + (i + 1) + "branco", typeof(Sprite)) as Sprite;

        for(int i = -2; i < 8; i++)
            this.b_pause[i + 2] = Resources.Load("Images/Game/Pause/b" + i, typeof(Sprite)) as Sprite; //Botões - Pause

        this.Background_Game = Resources.Load("Images/0 Comum/fundo_largo", typeof(Sprite)) as Sprite;
    }
}
