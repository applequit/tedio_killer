﻿using UnityEngine;
using System.Collections;

public class end_anim : MonoBehaviour
{

    Analytics_Manager analyticsManager;

    public bool finish;
    GameObject g;

    void Start()
    {

        this.analyticsManager = Analytics_Manager.getInstance();

        this.finish = false;
    }

    void Update()
    {

        if (GameObject.FindObjectOfType<Fade_Out>() == null)
            if (this.finish || Input.GetMouseButton(0))
                g = new GameObject("Fade_Out", typeof(Fade_Out));

        if (this.finish)
            this.analyticsManager.sendScene(100);
    }
}
