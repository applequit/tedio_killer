﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class UI_Cutscene : MonoBehaviour {
    
    private const float FRAME_DURATION = 2f;
    private const float FRAME_FADE_SPEED = .035f;
    private const float FRAME_FADE_DELAY = .01f;

    #region Cutscene Change
    public Sprite[] frames;
    private Image image;
	void Start () {
        this.image = this.GetComponent<Image>();
        StartCoroutine(ChangeFrame());
	}

    private IEnumerator ChangeFrame()
    {
        while (GameObject.Find("UI_FadeOut") != null)
            yield return new WaitForSeconds(.5f);
        foreach (Sprite frame in frames)
        {
            if (frame != null)
            {
                this.image.sprite = frame;
                if (frame != frames[0])
                {
                    for (float i = 0; i < 1; i += FRAME_FADE_SPEED)
                    {
                        if (i > 1)
                            i = 1;
                        this.image.color = new Color(this.image.color.r, this.image.color.g, this.image.color.b, i);
                        yield return new WaitForSeconds(FRAME_FADE_DELAY);
                    }
                }
                yield return new WaitForSeconds(FRAME_DURATION);
                if (frame != frames[frames.Length - 1])
                {
                    for (float i = 1; i > 0; i -= FRAME_FADE_SPEED)
                    {
                        if (i < 0)
                            i = 0;
                        this.image.color = new Color(this.image.color.r, this.image.color.g, this.image.color.b, i);
                        yield return new WaitForSeconds(FRAME_FADE_DELAY);
                    }
                }
            }
        }
        UI_FadeIn.GoToScene("Menu");
    }
    #endregion

    public void Skip()
    {
        UI_FadeIn.GoToScene("Menu");
    }
}
