﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Image))]
[RequireComponent(typeof(UI_MovableButton))]
[RequireComponent(typeof(Button))]
public class UI_ButtonBack : MonoBehaviour {

    private RectTransform rectTransform;
    private Image image;
    private UI_MovableButton mv;
    private Button button;
    private bool show = false;

    private bool started = false;
	void Start () {
        if (!started)
        {
            started = true;
            this.rectTransform = this.GetComponent<RectTransform>();
            this.image = this.GetComponent<Image>();
            this.mv = this.GetComponent<UI_MovableButton>();
            this.button = this.GetComponent<Button>();
            this.button.onClick.AddListener(() => { Hide(); });
        }
	}

    public IEnumerator Show()
    {
        //if (!show)
        {
            Start();
            gameObject.SetActive(true);
            this.image.color = new Color(this.image.color.r, this.image.color.g, this.image.color.b, 0);
            this.mv.IsStart = false;
            yield return new WaitForSeconds(0.7f);
            StartCoroutine(FadeIn(image));
            yield return new WaitForSeconds(0.06f);
            this.mv.IsStart = true;
            show = true;
        }
    }

    public void Hide()
    {
        //if (show)
        {
            this.mv.IsStart = false;
            show = false;
            StartCoroutine(FadeOut(image));
        }
    }
	

    IEnumerator FadeIn(Image img)
    {
        for (float i = 0; i < 1; i += .05f)
        {
            if (i > 1)
                i = 1;
            img.color = new Color(img.color.r, img.color.g, img.color.b, i);
            yield return new WaitForSeconds(.01f);
        }
        img.color = new Color(img.color.r, img.color.g, img.color.b, 1);
    }
    IEnumerator FadeOut(Image img)
    {
        for (float i = 1; i > 0; i -= .07f)
        {
            if (i < 0)
                i = 0;
            img.color = new Color(img.color.r, img.color.g, img.color.b, i);
            yield return new WaitForSeconds(.01f);
        }
        img.color = new Color(img.color.r, img.color.g, img.color.b, 0);
        gameObject.SetActive(false);
    }
}
