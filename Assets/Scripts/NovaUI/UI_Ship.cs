﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class UI_Ship : MonoBehaviour {

    private GameObject VFX = null;
    public bool IsSelected
    {
        get
        {
            return this.isSelected;
        }
        set
        {
            if (this.isSelected != value)
            {
                this.index = 0;
                this.isSelected = value;
            }
            if (this.VFX != null && value)
            {
                this.VFX.SetActive(true);
                UI_SelectFX u = this.VFX.GetComponent<UI_SelectFX>();
                u.StartCoroutine(u.Animate());
            }
        }
    }
    private bool isSelected = false;
    public Sprite[] Sprites;
    const float SPEED = .2f;
    private float index = 0;

    private Image spriteRenderer;

    void Start()
    {
        this.spriteRenderer = this.GetComponent<Image>();
        VFX = transform.Find("V").gameObject;
    }

	void FixedUpdate () {
        if (Sprites != null && IsSelected)
        {
            index += SPEED;
            if (index >= Sprites.Length)
                index = 0;
        }
	}

    void Update()
    {
        if(Sprites != null)
            this.spriteRenderer.sprite = Sprites[Mathf.FloorToInt(index)];
    }
}
