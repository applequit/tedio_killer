﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(RectTransform))]
public class UI_BtnRotateFX : MonoBehaviour
{
    const float TIMER_MAX = 1.2f;
    const float SPEED = 65f;

    public bool IsConfigWheel = false;

    private float side = -1f;
    private float timer = 0;
    private RectTransform rectTransform;
    private Button button;
    private bool activated = false;
    void Start()
    {
        this.rectTransform = this.GetComponent<RectTransform>();
        this.button = this.GetComponent<Button>();
        this.button.onClick.AddListener(() => { timer = TIMER_MAX; activated = true; });
    }

    

    void Update()
    {
        if (activated)
        {
            if (timer > 0)
            {
                this.rectTransform.Rotate(new Vector3(0, 0, side * Time.deltaTime * SPEED));
                timer -= Time.deltaTime;
            }
            else
            {
                if (!IsConfigWheel)
                {
                    timer = 0;
                    activated = false;
                }
                else
                {
                    if (side == -1)
                    {
                        timer = TIMER_MAX;
                        side *= -1;
                    }
                    else
                    {
                        activated = false;
                        timer = 0;
                        side = -1;
                    }
                }
            }
        }
    }
}
