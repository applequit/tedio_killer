﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(RectTransform))]
public class UI_MovableButton : MonoBehaviour {

    public GameObject END;

    private Vector3 StartPos;
    private Vector3 EndPos;

    public bool IsStart = true;

	void Start () {
        if (END != null)
        {
            StartPos = transform.position - transform.parent.position;
            EndPos = END.transform.position - transform.parent.position;
        }
	}
	
	void FixedUpdate () {
        Vector3 Target;
        if (IsStart)
            Target = StartPos;
        else
            Target = EndPos;
        Target += transform.parent.position;
        
        if (Target != null)
            transform.position = Vector3.Lerp(transform.position, Target, .1f);
	}
}
