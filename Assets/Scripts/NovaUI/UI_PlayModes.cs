﻿using UnityEngine;
using System.Collections;

public class UI_PlayModes : MonoBehaviour {

    private GameObject VFX = null;
    public void Select()
    {
        if (this.VFX != null)
        {
            this.VFX.SetActive(true);
            UI_SelectFX u = this.VFX.GetComponent<UI_SelectFX>();
            u.StartCoroutine(u.Animate());
        }
    }

    void Start()
    {
        VFX = transform.Find("V").gameObject;
    }
}
