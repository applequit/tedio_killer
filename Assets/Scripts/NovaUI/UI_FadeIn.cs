﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class UI_FadeIn : MonoBehaviour
{
    public string SceneToGo = "";
    private Image image;
    const float SPEED = 1.6f;
    private float alpha = 0f;

    // Use this for initialization
    void Start()
    {
        this.image = this.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.alpha < 1)
            this.alpha += SPEED * Time.deltaTime;
        else
        {
            if (SceneToGo != "")
                Application.LoadLevel(SceneToGo);
            else
                Destroy(this.gameObject);
        }
        if (this.alpha > 1)
            this.alpha = 1;
        this.image.color = new Color(0, 0, 0, alpha);
    }


    public static void GoToScene(string a)
    {
        GameObject canvas = GameObject.Find("Canvas");
        if (canvas != null)
        {
            GameObject o = new GameObject("UI_FadeIn", typeof(RectTransform), typeof(CanvasRenderer));
            UI_FadeIn i = o.AddComponent<UI_FadeIn>();
            o.transform.SetParent(canvas.transform);
            RectTransform t = o.GetComponent<RectTransform>();
            t.anchorMax = new Vector2(1, 1);
            t.anchorMin = new Vector2(0, 0);
            t.anchoredPosition = new Vector2(0, 0);
            Image img = o.GetComponent<Image>();
            img.color = new Color(0, 0, 0, 0);
            i.SceneToGo = a;
        }
    }
}
