﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI_ButtonFX : MonoBehaviour {

    private bool clicked = false;

    private RectTransform rectTransform;

    void Start()
    {
        this.rectTransform = this.GetComponent<RectTransform>();
    }
	
	void Update ()
    {
        
        //DebugDrawRect(rect, Color.red);
        if (((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)||Input.GetMouseButtonDown(0)) && !clicked)
        {
            Vector3 size = new Vector3(rectTransform.rect.width, rectTransform.rect.height, 0);
            Rect rect = new Rect(rectTransform.position - size / 2, size);
            if ((Input.touchCount > 0 && IsColliding(Input.GetTouch(0).position, rect)) || IsColliding(Input.mousePosition, rect))
            {
                rectTransform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
                clicked = true;
            }
        }
        if (((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0)) && clicked)
        {
            rectTransform.localScale = new Vector3(1f, 1f, 1f);
            clicked = false;
        }
    }

    bool IsColliding(Vector2 a, Rect b)
    {
        return a.x > b.x && a.x < b.x + b.width && a.y > b.y && a.y < b.y + b.height;
    }

    void DebugDrawRect(Rect rect, Color color)
    {
        Debug.DrawLine(new Vector3(rect.x, rect.y, 0), new Vector3(rect.x + rect.width, rect.y, 0), color);
        Debug.DrawLine(new Vector3(rect.x, rect.y + rect.height, 0), new Vector3(rect.x + rect.width, rect.y + rect.height, 0), color);
        Debug.DrawLine(new Vector3(rect.x, rect.y, 0), new Vector3(rect.x, rect.y + rect.height, 0), color);
        Debug.DrawLine(new Vector3(rect.x + rect.width, rect.y, 0), new Vector3(rect.x + rect.width, rect.y + rect.height, 0), color);
    }
}
