﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(RectTransform))]
public class UI_Controller : MonoBehaviour {

    public bool IsSelected
    {
        get
        {
            return this.isSelected;
        }
        set
        {
            if(value != this.isSelected)
            {
                if(this.rectTransform == null)
                    this.rectTransform = this.GetComponent<RectTransform>();
                this.isSelected = value;
                this.dir = 1;
                if (Mathf.RoundToInt(Random.Range(0, 2)) == 1)
                    this.dir *= -1;
                this.timer = TIMER_MAX;
                this.rectTransform.rotation = Quaternion.identity;
            }
        }
    }

    private bool isSelected = false;

    const float TIMER_MAX = .3f;
    const float SPEED = 17;

    private float dir = 1;
    private float timer = TIMER_MAX;
    private RectTransform rectTransform;
    void Start()
    {
        this.rectTransform = this.GetComponent<RectTransform>();
    }

    void Update()
    {
        if (this.IsSelected)
        {
            if (timer > 0)
                timer -= Time.deltaTime;
            else
            {
                timer = TIMER_MAX * 2f;
                dir *= -1;
            }
            this.rectTransform.Rotate(new Vector3(0, 0, Time.deltaTime * dir * SPEED));
        }
    }
}
