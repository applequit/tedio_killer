﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class UI_OnOff : MonoBehaviour {

    public bool IsOn
    {
        get
        {
            return this.isOn;
        }
        set
        {
            this.isOn = value;
            if (this.OnSprite != null && this.OffSprite != null)
            {
                if (this.image == null)
                    this.image = this.GetComponent<Image>();
                if (value)
                    this.image.sprite = this.OnSprite;
                else
                    this.image.sprite = this.OffSprite;
            }
        }
    }

    private bool isOn = true;
    public Sprite OnSprite;
    public Sprite OffSprite;

    private Image image;

	void Start () {
        this.image = GetComponent<Image>();
	}
}
