﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class UI_SelectFX : MonoBehaviour
{

    public Sprite[] Sprites;
    public float SPEED = .02f;

    private Image spriteRenderer;
    private bool running = false;

    void Start()
    {
        this.spriteRenderer = this.GetComponent<Image>();
    }

    public void ResetAnim()
    {
        running = false;
        this.spriteRenderer.sprite = Sprites[0];
        gameObject.SetActive(false);
    }

    public IEnumerator Animate()
    {
        if (!running)
        {
            running = true;
            yield return new WaitForSeconds(0.01f);
            foreach (Sprite spr in Sprites)
            {
                if (spr != null)
                    this.spriteRenderer.sprite = spr;
                if (running)
                    yield return new WaitForSeconds(SPEED);
                else
                    break;
            }
            this.spriteRenderer.sprite = Sprites[0];
            running = false;
            gameObject.SetActive(false);
        }
    }
}
