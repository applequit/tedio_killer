﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class UI_FadeOut : MonoBehaviour {

    private Image image;
    const float SPEED = 1.6f;
    private float alpha = 1f;

	// Use this for initialization
	void Start () {
        this.image = this.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (this.alpha > 0)
            this.alpha -= SPEED * Time.deltaTime;
        else
            Destroy(this.gameObject);
        if (this.alpha < 0)
            this.alpha = 0;
        this.image.color = new Color(this.image.color.r, this.image.color.g, this.image.color.b, alpha);
	}
}
